package com.company.poc.controller;

import com.company.poc.aplicativo.EjecutarSincronizacion;
import com.company.poc.aplicativo.ProcesaMapaDeCalculo;
import com.company.poc.configuracion.procesocalculo.ContextoDeAplicacion;
import com.company.poc.dominio.entity.Contract;
import com.company.poc.dominio.entity.Contract2;
import com.company.poc.dominio.entity.ContractHistory;
import com.company.poc.dominio.repositorio.Contract2Repositorio;
import com.company.poc.dominio.repositorio.ContractHistoryRepositorio;
import com.company.poc.dominio.repositorio.ContractRepositorio;
import com.company.poc.util.EmpresaEnum;
import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
public class BatchController {
    @Autowired
    private ContractRepositorio contractRepositorio;
    @Autowired
    private Contract2Repositorio contract2Repositorio;
    @Autowired
    private ContractHistoryRepositorio contractHistoryRepositorio;
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private Job job;
    @Autowired
    EjecutarSincronizacion administracionSincronizacion;
    //Modo Test
    @Autowired
    ContextoDeAplicacion contextoDeAplicacion;
    @Autowired
    ProcesaMapaDeCalculo mapaDeCalculo;


    public BatchController() {

    }

    @GetMapping("/insert")
    public String saveDummyData() {
        mapaDeCalculo.ejecuta(contextoDeAplicacion);
        List<Contract> contractList = new ArrayList<>();
        for (int i = 0; i < 5000; i++) {
            Contract contract = new Contract();
            contract.setHolderName("name-" + i);
            contract.setDuration(new Random().nextInt());
            contract.setAmount(new Random().nextInt(500000));
            LocalDateTime date = LocalDateTime.now();
            contract.setCreationDate(date);
            contract.setStatus("InProgress");
            contractList.add(contract);

        }
        contractRepositorio.saveAll(contractList);


        //---------------------------
        List<Contract2> contractList2 = new ArrayList<>();
        for (int i = 0; i < 5000; i++) {
            Contract2 contract2 = new Contract2();
            contract2.setHolderName("name-" + i);
            contract2.setDuration(new Random().nextInt());
            contract2.setAmount(new Random().nextInt(500000));
            LocalDateTime date = LocalDateTime.now();
            contract2.setCreationDate(date);
            contract2.setStatus("InProgress");
            contractList2.add(contract2);

        }
        contract2Repositorio.saveAll(contractList2);


        //----------------------------

        ContractHistory contractHistory = new ContractHistory();
        contractHistory.setContractId("aaaa");
        contractHistory.setHolderName("name-" + 1);
        contractHistory.setDuration(new Random().nextInt());
        contractHistory.setAmount(new Random().nextInt(500000));
        LocalDateTime date = LocalDateTime.now();
        contractHistory.setCreationDate(date);
        contractHistory.setStatus("test");

        contractHistoryRepositorio.save(contractHistory);
        return "saved successfully";
    }

    @GetMapping("/start-batch")
    @SneakyThrows
    public String startBatch(@RequestParam(defaultValue = "Coppel") String nombreEmpresa) throws JobExecutionAlreadyRunningException {
        String empresaLoc = EmpresaEnum.valueOf(nombreEmpresa) != null ? EmpresaEnum.valueOf(nombreEmpresa).name() : "Coppel";
        List<Long> ids = administracionSincronizacion.procesarSincronizacion(empresaLoc);
        return ids.stream().map(id -> new String("" + id)).collect(Collectors.joining(","));
    }

    @GetMapping("/reiniciar")
    public String reIniciarSincronizacion(@RequestParam String tabla, @RequestParam String mapaCalculo) throws Exception {
        return administracionSincronizacion.reIniciarSincronizacion(tabla, mapaCalculo);
    }


    @GetMapping("/iniciar-sincronizacion")
    public String iniciarSincronizacion(@RequestParam(defaultValue = "COPPEL") String nombreEmpresa) {
        administracionSincronizacion.procesarSincronizacion(nombreEmpresa);
        return "OK";
    }
}
