/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.procesocalculo;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 *
 * @author alvaro.ruiz
 */
public class AttributoParaContexto {

    private String name;
    private String expression;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public void addAttributeContext(StandardEvaluationContext context) {
        ExpressionParser parser = new SpelExpressionParser();
        Object valueResult = parser.parseExpression(getExpression()).getValue(context);
        context.setVariable(getName(), valueResult);
    }
}
