package com.company.poc.procesocalculo.servicios;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.util.HashMap;
import java.util.Map;

public class IdentificacionCanalServicio {
    private Map<String, String> reglasDeIdentificacion = new HashMap<>();
    private String valorPorDefault;

    public String identificacionCanal(int idTienda, int idCaja) {
        final ExpressionParser parser = new SpelExpressionParser();
        final String idTiendaStr = idTienda  > 0 ? "" + idTienda : "-1";
        final String idCajaStr = idCaja  > 0 ? "" + idCaja : "-1";
        return
                getReglasDeIdentificacion().entrySet().stream()
                        .filter(entryRegla -> parser.parseExpression(
                                entryRegla.getValue().replaceAll("idTienda", idTiendaStr).replace("idCaja", idCajaStr))
                                .getValue(Boolean.class))
                        .findFirst()
                        .map(entryRegla -> entryRegla.getKey())
                        .orElse(getValorPorDefault());
    }

    public Map<String, String> getReglasDeIdentificacion() {
        return reglasDeIdentificacion;
    }

    public void setReglasDeIdentificacion(Map<String, String> reglasDeIdentificacion) {
        this.reglasDeIdentificacion = reglasDeIdentificacion;
    }
    public String getValorPorDefault() {
        return valorPorDefault;
    }

    public void setValorPorDefault(String valorPorDefault) {
        this.valorPorDefault = valorPorDefault;
    }

}
