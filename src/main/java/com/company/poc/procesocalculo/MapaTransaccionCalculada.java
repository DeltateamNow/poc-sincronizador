/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.procesocalculo;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author alvaro.ruiz
 */
public class MapaTransaccionCalculada {

    private String expresionDeCalculo;
    private String atributoCalculado;
    private List<MapaTransaccionCalculada> datosCalculado = new ArrayList<>();
    private List<AttributoParaContexto> valoresAdicionales = new ArrayList<>();

    /**
     * @param context    Contexto a utilizarse para las Expresiones de tipo Spring
     * @param nombreMapa Identificador del Mapa de salida, para agregar los
     *                   atributos de salida
     * @return Mapa resultante con los cálculos y atributos que fueron
     * configurados
     */
    public Map<String, Object> aplicarMapaCalculo(StandardEvaluationContext context, String nombreMapa) {
        ExpressionParser parser = new SpelExpressionParser();
        Map<String, Object> resultado = null;
        Object result = null;
        // Proceso que permite agregar variable calculada en el contexto, para utilizarse en el proceso del mapeo del cálculo
        synchronized (this) {
            try {
                getValoresAdicionales().forEach(att -> {
                    ExpressionParser expressionParser = new SpelExpressionParser();
                    Object resultadoAtt = expressionParser.parseExpression(att.getExpression()).getValue(context);
                    context.setVariable(att.getName(), resultadoAtt);
                });
                Object mapeoResultado = null;

                resultado = parser.parseExpression("[" + nombreMapa + "]").getValue(context, Map.class);
                mapeoResultado = parser.parseExpression(expresionDeCalculo).getValue(context);
                resultado.put(atributoCalculado, mapeoResultado);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        getDatosCalculado().stream()
                .forEach(datoCalculo -> datoCalculo.aplicarMapaCalculo(context, nombreMapa));

        return resultado;
    }

    public String getExpresionDeCalculo() {
        return expresionDeCalculo;
    }

    public void setExpresionDeCalculo(String expresionDeCalculo) {
        this.expresionDeCalculo = expresionDeCalculo;
    }

    public String getAtributoCalculado() {
        return atributoCalculado;
    }

    public void setAtributoCalculado(String atributoCalculado) {
        this.atributoCalculado = atributoCalculado;
    }

    public List<MapaTransaccionCalculada> getDatosCalculado() {
        return datosCalculado;
    }

    public void setDatosCalculado(List<MapaTransaccionCalculada> datosCalculado) {
        this.datosCalculado = datosCalculado;
    }

    public List<AttributoParaContexto> getValoresAdicionales() {
        return valoresAdicionales;
    }

    public void setValoresAdicionales(List<AttributoParaContexto> valoresAdicionales) {
        this.valoresAdicionales = valoresAdicionales;
    }
}
