/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.procesocalculo;

import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author alvaro.ruiz
 */
public class MapaDeCalculo {
    private String NombreMapaCalculo;
    private List<MapaTransaccionCalculada> mapaTransaccionCalculadas;
    private List<MapaDeCalculo> mapaDeMultipleSalidas;

    /**
     *
     * @param map  Registro de Origen
     * @param mapPOC  Contenedor con los objetos a utilizarse en el contexto
     * @return
     */
    public List<Map<String, Object>> aplicarMapaDeDatos(final Map<String, Object> map, final Map<String, Object> mapPOC) {
        List<MapaDeCalculo> mapaDeMultipleSalidasLocal;
        if (mapaDeMultipleSalidas == null) {
            mapaDeMultipleSalidasLocal = new ArrayList<>();
            mapaDeMultipleSalidasLocal.add(this);
        } else {
            mapaDeMultipleSalidasLocal = mapaDeMultipleSalidas;
        }

        return mapaDeMultipleSalidasLocal
                .stream()
                .map(transaccionCalculada ->
                        transaccionCalculada.mapaTransaccionCalculadas
                                .parallelStream()
                                .reduce(new ConcurrentHashMap<>(), (parRecord, txCalculada) -> {
                                    Map<String, Object> internalMap = new ConcurrentHashMap<>(mapPOC);
                                    internalMap.put("registroOrigen", map);
                                    internalMap.put(NombreMapaCalculo, new ConcurrentHashMap<>());
                                    StandardEvaluationContext contextoDeMapeo = new StandardEvaluationContext(internalMap);
                                    return txCalculada.aplicarMapaCalculo(contextoDeMapeo, NombreMapaCalculo);
                                }, MapaDeCalculo::uneMapas))
                .collect(Collectors.toList());
    }

    /*
        Método de apoyo para el proceso de reducción en el paralelo y unir los resultados
     */
    public static Map<String, Object> uneMapas(Map<String, Object> mapaAquienUne, Map<String, Object> mapaParaUnir) {
        mapaAquienUne.putAll(mapaParaUnir);
        return mapaAquienUne;
    }

    public String getNombreMapaCalculo() {
        return NombreMapaCalculo;
    }

    public void setNombreMapaCalculo(String nombreMapaCalculo) {
        NombreMapaCalculo = nombreMapaCalculo;
    }

    public List<MapaTransaccionCalculada> getMapaTransaccionCalculadas() {
        return mapaTransaccionCalculadas;
    }

    public void setMapaTransaccionCalculadas(List<MapaTransaccionCalculada> mapaTransaccionCalculadas) {
        this.mapaTransaccionCalculadas = mapaTransaccionCalculadas;
    }

    public List<MapaDeCalculo> getMapaDeMultipleSalidas() {
        return mapaDeMultipleSalidas;
    }

    public void setMapaDeMultipleSalidas(List<MapaDeCalculo> mapaDeMultipleSalidas) {
        this.mapaDeMultipleSalidas = mapaDeMultipleSalidas;
    }


}
