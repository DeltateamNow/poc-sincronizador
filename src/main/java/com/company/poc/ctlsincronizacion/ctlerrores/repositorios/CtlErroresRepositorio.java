package com.company.poc.ctlsincronizacion.ctlerrores.repositorios;

import com.company.poc.ctlsincronizacion.ctlerrores.entidades.CtlErroresSincronizacion;

public interface CtlErroresRepositorio {
    public void registrarErrorSincronizacion(CtlErroresSincronizacion ctlErroresSincronizacion);
}
