package com.company.poc.ctlsincronizacion.ctlerrores.entidades;

import com.company.poc.dominio.catalogos.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "CtlErroresSincronizacion")
@Component
public class CtlErroresSincronizacion {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatEmpresaId", nullable = false)
    private CatEmpresa catEmpresaId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatCanalId", nullable = false)
    private CatCanal catCanalId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatProductoId", nullable = false)
    private CatProducto catProductoId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatTipoTransacionId", nullable = false)
    private CatTipoTransaccion catTipoTransacionId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatTiendaId", nullable = false)
    private CatTienda catTiendaId;

    @Column(name = "CatCajaId")
    private Long catCajaId;

    @Lob
    @Column(name = "JsonTransaccionOmnicanal")
    private String jsonTransaccionOmnicanal;

    @Column(name = "DesMotivo", length = 250)
    private String desMotivo;

    @Column(name = "NumReintentos")
    private Integer numReintentos;

    @Column(name = "FecRegistro", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecRegistro;

    @Column(name = "FlaEnvioRealizado", nullable = false)
    private Boolean flaEnvioRealizado = false;

    public Boolean getFlaEnvioRealizado() {
        return flaEnvioRealizado;
    }

    public void setFlaEnvioRealizado(Boolean flaEnvioRealizado) {
        this.flaEnvioRealizado = flaEnvioRealizado;
    }

    public LocalDateTime getFecRegistro() {
        return fecRegistro;
    }

    public void setFecRegistro(LocalDateTime fecRegistro) {
        this.fecRegistro = fecRegistro;
    }

    public Integer getNumReintentos() {
        return numReintentos;
    }

    public void setNumReintentos(Integer numReintentos) {
        this.numReintentos = numReintentos;
    }

    public String getDesMotivo() {
        return desMotivo;
    }

    public void setDesMotivo(String desMotivo) {
        this.desMotivo = desMotivo;
    }

    public String getJsonTransaccionOmnicanal() {
        return jsonTransaccionOmnicanal;
    }

    public void setJsonTransaccionOmnicanal(String jsonTransaccionOmnicanal) {
        this.jsonTransaccionOmnicanal = jsonTransaccionOmnicanal;
    }

    public Long getCatCajaId() {
        return catCajaId;
    }

    public void setCatCajaId(Long catCajaId) {
        this.catCajaId = catCajaId;
    }

    public CatTienda getCatTiendaId() {
        return catTiendaId;
    }

    public void setCatTiendaId(CatTienda catTiendaId) {
        this.catTiendaId = catTiendaId;
    }

    public CatTipoTransaccion getCatTipoTransacionId() {
        return catTipoTransacionId;
    }

    public void setCatTipoTransacionId(CatTipoTransaccion catTipoTransacionId) {
        this.catTipoTransacionId = catTipoTransacionId;
    }

    public CatProducto getCatProductoId() {
        return catProductoId;
    }

    public void setCatProductoId(CatProducto catProductoId) {
        this.catProductoId = catProductoId;
    }

    public CatCanal getCatCanalId() {
        return catCanalId;
    }

    public void setCatCanalId(CatCanal catCanalId) {
        this.catCanalId = catCanalId;
    }

    public CatEmpresa getCatEmpresaId() {
        return catEmpresaId;
    }

    public void setCatEmpresaId(CatEmpresa catEmpresaId) {
        this.catEmpresaId = catEmpresaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}