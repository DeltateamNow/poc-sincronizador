package com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.repositorios;

import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.entidades.CtlSincronizacionesDiario;

public interface CtlSincronizacionDiarioRepositorio {
    public void registrarSincronizacionDiaria(CtlSincronizacionesDiario ctlSincronizacionDiarioRepositorio);
    public boolean tablaEnEjecucionConDatos(String tabla);
}
