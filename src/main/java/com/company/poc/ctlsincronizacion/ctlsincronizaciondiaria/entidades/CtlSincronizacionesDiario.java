package com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.entidades;

import com.company.poc.dominio.catalogos.*;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "CtlSincronizacionesDiario")
public class CtlSincronizacionesDiario {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatEmpresaId", nullable = false)
    private CatEmpresa catEmpresaId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatCanalId", nullable = false)
    private CatCanal catCanalId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatProductoId", nullable = false)
    private CatProducto catProductoId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatTipoTransacionId", nullable = false)
    private CatTipoTransaccion catTipoTransacionId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CatTiendaId", nullable = false)
    private CatTienda catTiendaId;

    @Column(name = "NumTasaImpuesto", nullable = false)
    private Integer numTasaImpuesto;

    @Column(name = "NumTransaccionOrigen", nullable = false)
    private Long numTransaccionOrigen;

    @Column(name = "NumTransaccionOmnicanal", nullable = false)
    private Long numTransaccionOmnicanal;

    @Column(name = "ImpConImpuesto", nullable = false, precision = 14, scale = 2)
    private BigDecimal impConImpuesto;

    @Column(name = "ImpSinImpuesto", nullable = false, precision = 14, scale = 2)
    private BigDecimal impSinImpuesto;

    @Column(name = "FecSincronizacion", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecSincronizacion;

    @Column(name = "FecControl", nullable = false)
    private LocalDateTime fecControl;

    public LocalDateTime getFecControl() {
        return fecControl;
    }

    public void setFecControl(LocalDateTime fecControl) {
        this.fecControl = fecControl;
    }

    public LocalDateTime getFecSincronizacion() {
        return fecSincronizacion;
    }

    public void setFecSincronizacion(LocalDateTime fecSincronizacion) {
        this.fecSincronizacion = fecSincronizacion;
    }

    public BigDecimal getImpSinImpuesto() {
        return impSinImpuesto;
    }

    public void setImpSinImpuesto(BigDecimal impSinImpuesto) {
        this.impSinImpuesto = impSinImpuesto;
    }

    public BigDecimal getImpConImpuesto() {
        return impConImpuesto;
    }

    public void setImpConImpuesto(BigDecimal impConImpuesto) {
        this.impConImpuesto = impConImpuesto;
    }

    public Long getNumTransaccionOmnicanal() {
        return numTransaccionOmnicanal;
    }

    public void setNumTransaccionOmnicanal(Long numTransaccionOmnicanal) {
        this.numTransaccionOmnicanal = numTransaccionOmnicanal;
    }

    public Long getNumTransaccionOrigen() {
        return numTransaccionOrigen;
    }

    public void setNumTransaccionOrigen(Long numTransaccionOrigen) {
        this.numTransaccionOrigen = numTransaccionOrigen;
    }

    public Integer getNumTasaImpuesto() {
        return numTasaImpuesto;
    }

    public void setNumTasaImpuesto(Integer numTasaImpuesto) {
        this.numTasaImpuesto = numTasaImpuesto;
    }

    public CatTienda getCatTiendaId() {
        return catTiendaId;
    }

    public void setCatTiendaId(CatTienda catTiendaId) {
        this.catTiendaId = catTiendaId;
    }

    public CatTipoTransaccion getCatTipoTransacionId() {
        return catTipoTransacionId;
    }

    public void setCatTipoTransacionId(CatTipoTransaccion catTipoTransacionId) {
        this.catTipoTransacionId = catTipoTransacionId;
    }

    public CatProducto getCatProductoId() {
        return catProductoId;
    }

    public void setCatProductoId(CatProducto catProductoId) {
        this.catProductoId = catProductoId;
    }

    public CatCanal getCatCanalId() {
        return catCanalId;
    }

    public void setCatCanalId(CatCanal catCanalId) {
        this.catCanalId = catCanalId;
    }

    public CatEmpresa getCatEmpresaId() {
        return catEmpresaId;
    }

    public void setCatEmpresaId(CatEmpresa catEmpresaId) {
        this.catEmpresaId = catEmpresaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}