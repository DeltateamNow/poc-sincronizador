package com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.servicios;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.EstatusSincronizacion;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.repositorios.CtlSincronizacionDiarioRepositorio;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ControlDiarioSincronizacionServicio {

    private CtlSincronizacionDiarioRepositorio ctlSincronizacionDiarioRepositorio;


    public void setCtlSincronizacionDiarioRepositorio(CtlSincronizacionDiarioRepositorio ctlSincronizacionDiarioRepositorio) {
        this.ctlSincronizacionDiarioRepositorio = ctlSincronizacionDiarioRepositorio;
    }


    public synchronized boolean tablaOrigenConRegistros(JobParameters parameters) {
        return ctlSincronizacionDiarioRepositorio.tablaEnEjecucionConDatos(parameters.getString("tabla"));
    }

    public void actualizarAgenteCompletado(JobExecution jobExecution, EstatusSincronizacion estatusSincronizacion) {
        // Basándonos en el id se toma el Mapa del agente y se actualiza su estatus
        // Cambia el Estatus de acuerdo en base el estatus
        // Se remueve el registro del Mapa

        // Se realiza llamada al store procedure para acumulado de las métricas
        // Pasar Empresa, Producto, Clave Movimiento y Clave Tipo Movimiento

    }



}
