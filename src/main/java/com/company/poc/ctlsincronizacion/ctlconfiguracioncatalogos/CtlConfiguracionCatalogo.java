package com.company.poc.ctlsincronizacion.ctlconfiguracioncatalogos;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class CtlConfiguracionCatalogo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @Column(name = "NomCatalogoOrigen", nullable = false, length = 50)
    private String nomCatalogoOrigen;

    @Column(name = "NomAtributoFiltro", nullable = false, length = 50)
    private String nomAtributoFiltro;

    @Column(name = "DesOperacionFiltro", nullable = false, length = 50)
    private String desOperacionFiltro;

    @Column(name = "NumControl", nullable = false)
    private Long numControl;

    @Column(name = "FecUltimaActualizacion", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecUltimaActualizacion;

    @Column(name = "NomCatalogoDestino", nullable = false, length = 50)
    private String nomCatalogoDestino;

    @Column(name = "NomAtributos", nullable = false, length = 100)
    private String nomAtributos;

    public String getNomAtributos() {
        return nomAtributos;
    }

    public void setNomAtributos(String nomAtributos) {
        this.nomAtributos = nomAtributos;
    }

    public String getNomCatalogoDestino() {
        return nomCatalogoDestino;
    }

    public void setNomCatalogoDestino(String nomCatalogoDestino) {
        this.nomCatalogoDestino = nomCatalogoDestino;
    }

    public LocalDateTime getFecUltimaActualizacion() {
        return fecUltimaActualizacion;
    }

    public void setFecUltimaActualizacion(LocalDateTime fecUltimaActualizacion) {
        this.fecUltimaActualizacion = fecUltimaActualizacion;
    }

    public Long getNumControl() {
        return numControl;
    }

    public void setNumControl(Long numControl) {
        this.numControl = numControl;
    }

    public String getDesOperacionFiltro() {
        return desOperacionFiltro;
    }

    public void setDesOperacionFiltro(String desOperacionFiltro) {
        this.desOperacionFiltro = desOperacionFiltro;
    }

    public String getNomAtributoFiltro() {
        return nomAtributoFiltro;
    }

    public void setNomAtributoFiltro(String nomAtributoFiltro) {
        this.nomAtributoFiltro = nomAtributoFiltro;
    }

    public String getNomCatalogoOrigen() {
        return nomCatalogoOrigen;
    }

    public void setNomCatalogoOrigen(String nomCatalogoOrigen) {
        this.nomCatalogoOrigen = nomCatalogoOrigen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}