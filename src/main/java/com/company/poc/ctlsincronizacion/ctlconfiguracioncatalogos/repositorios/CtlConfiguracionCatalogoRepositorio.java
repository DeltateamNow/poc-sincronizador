package com.company.poc.ctlsincronizacion.ctlconfiguracioncatalogos.repositorios;

import java.time.LocalDateTime;

public interface CtlConfiguracionCatalogoRepositorio {
    public boolean catalogoActualizado(LocalDateTime dateTime, String catalogo);
}
