package com.company.poc.ctlsincronizacion.ctlentrega.servicio;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.servicios.AdministrarControlDeEjecucion;
import com.company.poc.ctlsincronizacion.ctlentrega.repositorios.MovTransaccionOmnicanalRepositorio;
import com.company.poc.ctlsincronizacion.ctlentrega.repositorios.MovTransaccionOrigenRepositorio;
import com.company.poc.dominio.entity.ContractHistory;
import com.company.poc.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AlmacenarTransaccionesCalculadas {
    private Map<String, List<String>> contenedores = new ConcurrentHashMap<>();
    private List<Future> enviosAsyncOmnicanal = Collections.synchronizedList(new ArrayList<Future>());
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private AtomicInteger registrosAfectados = new AtomicInteger();
    private final int NUM_MAXIMO_TX_CALCULADAS = 2000;

    private MovTransaccionOmnicanalRepositorio movTransaccionOmnicanalRepositorio;
    private MovTransaccionOrigenRepositorio movTransaccionOrigenRepositorio;

    private AdministrarControlDeEjecucion administrarControlDeEjecucion;

    @Autowired
    EntregaTransaccionOmnicanal entregaTransaccionOmnicanal;

    public AlmacenarTransaccionesCalculadas(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void inicializaContendor(String tabla) {
        contenedores.put(tabla, Collections.synchronizedList(new ArrayList<String>()));
    }

    public void terminaContendor(String tabla) {
        contenedores.get(tabla).clear();
    }

    public void addItemsAContenedor(List<? extends String> items, String tabla, Long agenteId) {
        if (contenedores.get(tabla).size() > NUM_MAXIMO_TX_CALCULADAS) {
            List<String> itemsParaEntrega = contenedores.get(tabla);
            inicializaContendor(tabla);
            contenedores.get(tabla).addAll(items);
            enviosAsyncOmnicanal.add(
                    CompletableFuture.runAsync(() -> {
                        entregaTransaccionOmnicanal.enviarTxOmnicanal(itemsParaEntrega, agenteId);
                        Util.chunked(itemsParaEntrega.stream(), 1000)
                                .forEach(chunk -> actualizaDB(chunk, tabla));
                    }));
        } else {
            contenedores.get(tabla).addAll(items);
        }
    }

    public void ejecutaPersistencia(String tabla, Long agenteId) {

        List<String> listaParaPersistir = contenedores.get(tabla);
        //Verifica que todos los envíos estén entregados
        enviosAsyncOmnicanal.stream().forEach(future -> {
            try {
                future.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        //Realiza el envío del último segmento
        entregaTransaccionOmnicanal.enviarTxOmnicanal(listaParaPersistir, agenteId);
        Util.chunked(listaParaPersistir.stream(), 1000)
                .forEach(chunk -> actualizaDB(chunk, tabla));
    }

    @Transactional
    private void procederActualizarDatos(List<String> subList, String tabla) {

        obtenerInstanciasTablaDestino(subList).forEach(
                ch -> {
                    try {
                        final String INSERT_QUERY = "INSERT INTO ContractHistory (contractId, amount, creationDate, duration," +
                                "holderName, status) VALUES ( :contractId, :amount, :creationDate, :duration, :holderName, 'EFFECTIVE')";
                        SqlParameterSource sqlParameterSource = new BeanPropertySqlParameterSource(ch);
                        jdbcTemplate.update(INSERT_QUERY, sqlParameterSource);
                        System.out.println("INSERT INTO ContractHistory....");
                        int registros = registrosAfectados.incrementAndGet();

                        /* Realizar el delete de los registro hasta que se halla terminado el proceso
                           Se programa dos horas del pues del la última ejecución, para evitar el problema de bloqueos
                         */
                        /*
                        final String DELETE_QUERY = "DELETE FROM " + tabla + " WHERE contractId IN (:contractId)";
                        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
                        parameterSource.addValue("contractId", ch.getContractId());
                        jdbcTemplate.update(DELETE_QUERY, parameterSource);

                        System.out.println("DELETE FROM " + tabla + "...." + registros);

                         */

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                });
    }

    @Transactional
    public void actualizaDB(List<? extends String> items, String tablaOrigen) {

        List<ContractHistory> itemsContract = obtenerInstanciasTablaDestino(items);

        SqlParameterSource[] sqlParameterSource = new SqlParameterSource[itemsContract.size()];
        AtomicInteger ind = new AtomicInteger(0);
        itemsContract.stream().forEach(item -> {
            sqlParameterSource[ind.getAndIncrement()] = new BeanPropertySqlParameterSource(item);
        });

        final String INSERT_QUERY = "INSERT INTO ContractHistory (contractId, amount, creationDate, duration," +
                "holderName, status) VALUES ( :contractId, :amount, :creationDate, :duration, :holderName, 'EFFECTIVE')";
        jdbcTemplate.batchUpdate(INSERT_QUERY, sqlParameterSource);

        List<String> contractIdList = itemsContract.stream().map(ch -> ch.getContractId()).collect(Collectors.toList());
        final String DELETE_QUERY = "DELETE FROM " + tablaOrigen + " WHERE contractId IN (:contractId)";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("contractId", contractIdList);
        jdbcTemplate.update(DELETE_QUERY, parameterSource);


        int registros = registrosAfectados.addAndGet(items.size());

        log.info("registros ----> " + registros);

    }


    private synchronized List<ContractHistory> obtenerInstanciasTablaDestino(List<? extends String> items) {
        return items.stream().map(msj -> msj.split(Util.SEPARADOR_MENSAJES)[0])
                .map(Util::getInstanceFromJson)
                .collect(Collectors.toList());
    }

    public MovTransaccionOmnicanalRepositorio getMovTransaccionOmnicanalRepositorio() {
        return movTransaccionOmnicanalRepositorio;
    }

    public void setMovTransaccionOmnicanalRepositorio(MovTransaccionOmnicanalRepositorio movTransaccionOmnicanalRepositorio) {
        this.movTransaccionOmnicanalRepositorio = movTransaccionOmnicanalRepositorio;
    }

    public MovTransaccionOrigenRepositorio getMovTransaccionOrigenRepositorio() {
        return movTransaccionOrigenRepositorio;
    }

    public void setMovTransaccionOrigenRepositorio(MovTransaccionOrigenRepositorio movTransaccionOrigenRepositorio) {
        this.movTransaccionOrigenRepositorio = movTransaccionOrigenRepositorio;
    }

    public AdministrarControlDeEjecucion getAdministrarControlDeEjecucion() {
        return administrarControlDeEjecucion;
    }

    public void setAdministrarControlDeEjecucion(AdministrarControlDeEjecucion administrarControlDeEjecucion) {
        this.administrarControlDeEjecucion = administrarControlDeEjecucion;
    }
}
