package com.company.poc.ctlsincronizacion.ctlentrega.entidades;

import javax.persistence.*;

@Entity
@Table(name = "MovTransaccionOmnicanal")
public class MovTransaccionOmnicanal {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Lob
    @Column(name = "TransaccionJson", nullable = false)
    private String transaccionJson;

    public String getTransaccionJson() {
        return transaccionJson;
    }

    public void setTransaccionJson(String transaccionJson) {
        this.transaccionJson = transaccionJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}