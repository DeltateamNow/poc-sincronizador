package com.company.poc.ctlsincronizacion.ctlentrega.entidades;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "MovTransaccionOrigen")
public class MovTransaccionOrigen {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NomOrigen", length = 50, nullable = false)
    private String nomOrigen;
    @Column(name = "ClvMovimiento", length = 1, nullable = false)
    private String cvlMovimiento;
    @Column(name = "ClvTipoMovimiento", length = 1, nullable = false)
    private String cvlTipoMovimiento;
    @Column(name = "NumTienda", nullable = false)
    private int numTienda;
    @Column(name = "FecTransaccion", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecTransaccion;
    @Lob
    @Column(name = "TransaccionJson", nullable = false)
    private String transaccionJson;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomOrigen() {
        return nomOrigen;
    }

    public void setNomOrigen(String nomOrigen) {
        this.nomOrigen = nomOrigen;
    }

    public String getCvlMovimiento() {
        return cvlMovimiento;
    }

    public void setCvlMovimiento(String cvlMovimiento) {
        this.cvlMovimiento = cvlMovimiento;
    }

    public String getCvlTipoMovimiento() {
        return cvlTipoMovimiento;
    }

    public void setCvlTipoMovimiento(String cvlTipoMovimiento) {
        this.cvlTipoMovimiento = cvlTipoMovimiento;
    }

    public int getNumTienda() {
        return numTienda;
    }

    public void setNumTienda(int numTienda) {
        this.numTienda = numTienda;
    }

    public LocalDateTime getFecTransaccion() {
        return fecTransaccion;
    }

    public void setFecTransaccion(LocalDateTime fecTransaccion) {
        this.fecTransaccion = fecTransaccion;
    }

    public String getTransaccionJson() {
        return transaccionJson;
    }

    public void setTransaccionJson(String transaccionJson) {
        this.transaccionJson = transaccionJson;
    }
}