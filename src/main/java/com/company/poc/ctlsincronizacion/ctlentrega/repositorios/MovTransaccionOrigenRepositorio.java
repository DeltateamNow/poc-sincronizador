package com.company.poc.ctlsincronizacion.ctlentrega.repositorios;

import java.util.List;

public interface MovTransaccionOrigenRepositorio {
    public void removerRegistroOrigen(Long id);
    public void removerRegistrosOrigen(List<Long> ids);
}
