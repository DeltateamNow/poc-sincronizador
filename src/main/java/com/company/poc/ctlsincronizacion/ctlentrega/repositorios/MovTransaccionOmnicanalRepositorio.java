package com.company.poc.ctlsincronizacion.ctlentrega.repositorios;

import com.company.poc.ctlsincronizacion.ctlentrega.entidades.MovTransaccionOmnicanal;

import java.util.List;

public interface MovTransaccionOmnicanalRepositorio {
    public void registrarTransaccionOmnicanal(MovTransaccionOmnicanal movTransaccionOmnicanal);
    public void registrarTransaccionesOmnicanal(List<MovTransaccionOmnicanal> movTransaccionesOmnicanal);
}
