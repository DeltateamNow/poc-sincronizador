package com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades;

import com.company.poc.dominio.catalogos.CatEmpresa;
import com.company.poc.dominio.catalogos.CatProducto;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "CtlAgenteSincronizador")
public class CtlAgenteSincronizador {
    @Id
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CtlConfiguracionSincronizadorId")
    private CtlConfiguracionSincronizador ctlConfiguracionSincronizadorId;

    @ManyToOne
    @JoinColumn(name = "CatEmpresaId")
    private CatEmpresa catEmpresaId;

    @ManyToOne
    @JoinColumn(name = "CatProductoId")
    private CatProducto catProductoId;

    @Column(name = "NomAgente", nullable = false, length = 50)
    private String nomAgente;

    @Column(name = "NomOrigen", nullable = false, length = 50)
    private String nomOrigen;

    @Column(name = "ClvMovimiento", nullable = false, length = 1)
    private String clvMovimiento;

    @Column(name = "ClvTipoMovimiento", nullable = false, length = 1)
    private String clvTipoMovimiento;

    @Column(name = "FecInicioSincronizacion", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecInicioSincronizacion;

    @Column(name = "FecFinSincronizacion", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecFinSincronizacion;

    @Column(name = "FecControl", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime fecControl;

    @Column(name = "OpcEstatusSincronizacion", nullable = false)
    private String opcEstatusSincronizacion;

    @Column(name = "OpcActivo", nullable = false)
    private boolean opcActivo;

    public LocalDateTime getFecControl() {
        return fecControl;
    }

    public void setFecControl(LocalDateTime fecControl) {
        this.fecControl = fecControl;
    }

    public LocalDateTime getFecFinSincronizacion() {
        return fecFinSincronizacion;
    }

    public void setFecFinSincronizacion(LocalDateTime fecFinSincronizacion) {
        this.fecFinSincronizacion = fecFinSincronizacion;
    }

    public LocalDateTime getFecInicioSincronizacion() {
        return fecInicioSincronizacion;
    }

    public void setFecInicioSincronizacion(LocalDateTime fecInicioSincronizacion) {
        this.fecInicioSincronizacion = fecInicioSincronizacion;
    }

    public String getClvTipoMovimiento() {
        return clvTipoMovimiento;
    }

    public void setClvTipoMovimiento(String clvTipoMovimiento) {
        this.clvTipoMovimiento = clvTipoMovimiento;
    }

    public String getClvMovimiento() {
        return clvMovimiento;
    }

    public void setClvMovimiento(String clvMovimiento) {
        this.clvMovimiento = clvMovimiento;
    }

    public String getNomOrigen() {
        return nomOrigen;
    }

    public void setNomOrigen(String nomOrigen) {
        this.nomOrigen = nomOrigen;
    }

    public String getNomAgente() {
        return nomAgente;
    }

    public void setNomAgente(String nomAgente) {
        this.nomAgente = nomAgente;
    }

    public CatProducto getCatProductoId() {
        return catProductoId;
    }

    public void setCatProductoId(CatProducto catProductoId) {
        this.catProductoId = catProductoId;
    }

    public CatEmpresa getCatEmpresaId() {
        return catEmpresaId;
    }

    public void setCatEmpresaId(CatEmpresa catEmpresaId) {
        this.catEmpresaId = catEmpresaId;
    }

    public CtlConfiguracionSincronizador getCtlConfiguracionSincronizadorId() {
        return ctlConfiguracionSincronizadorId;
    }

    public void setCtlConfiguracionSincronizadorId(CtlConfiguracionSincronizador ctlConfiguracionSincronizadorId) {
        this.ctlConfiguracionSincronizadorId = ctlConfiguracionSincronizadorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpcEstatusSincronizacion() {
        return opcEstatusSincronizacion;
    }

    public void setOpcEstatusSincronizacion(String opcEstatusSincronizacion) {
        this.opcEstatusSincronizacion = opcEstatusSincronizacion;
    }

    public boolean isOpcActivo() {
        return opcActivo;
    }

    public void setOpcActivo(boolean opcActivo) {
        this.opcActivo = opcActivo;
    }
}