package com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades;

import com.company.poc.dominio.catalogos.CatEmpresa;
import com.company.poc.dominio.catalogos.CatProducto;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "CtlConfiguracionSincronizador")
public class CtlConfiguracionSincronizador {
    @Id
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Column(name = "NomOrigen", nullable = false, length = 50)
    private String nomOrigen;

    @Column(name = "ClvMovimiento", nullable = false, length = 1)
    private String clvMovimiento;

    @Column(name = "ClvTipoMovimiento", nullable = false, length = 1)
    private String clvTipoMovimiento;

    @ManyToOne
    @JoinColumn(name = "CatEmpresaId")
    private CatEmpresa catEmpresaId;

    @ManyToOne
    @JoinColumn(name = "CatProductoId")
    private CatProducto catProductoId;

    @Column(name = "DesTransaccion", nullable = false, length = 100)
    private String desTransaccion;

    @Column(name = "NomAgente", nullable = false, length = 50)
    private String nomAgente;

    @Column(name = "HrsInicio", nullable = false)
    private Instant hrsInicio;

    @Column(name = "HrsFin", nullable = false)
    private Instant hrsFin;

    @Column(name = "NumPeriodicIdad", nullable = false)
    private Integer numPeriodicIdad;

    @Column(name = "OpcActivo", nullable = false)
    private Boolean opcActivo = false;

    public Boolean getOpcActivo() {
        return opcActivo;
    }

    public void setOpcActivo(Boolean opcActivo) {
        this.opcActivo = opcActivo;
    }

    public Integer getNumPeriodicIdad() {
        return numPeriodicIdad;
    }

    public void setNumPeriodicIdad(Integer numPeriodicIdad) {
        this.numPeriodicIdad = numPeriodicIdad;
    }

    public Instant getHrsFin() {
        return hrsFin;
    }

    public void setHrsFin(Instant hrsFin) {
        this.hrsFin = hrsFin;
    }

    public Instant getHrsInicio() {
        return hrsInicio;
    }

    public void setHrsInicio(Instant hrsInicio) {
        this.hrsInicio = hrsInicio;
    }

    public String getNomAgente() {
        return nomAgente;
    }

    public void setNomAgente(String nomAgente) {
        this.nomAgente = nomAgente;
    }

    public String getDesTransaccion() {
        return desTransaccion;
    }

    public void setDesTransaccion(String desTransaccion) {
        this.desTransaccion = desTransaccion;
    }

    public CatProducto getCatProductoId() {
        return catProductoId;
    }

    public void setCatProductoId(CatProducto catProductoId) {
        this.catProductoId = catProductoId;
    }

    public CatEmpresa getCatEmpresaId() {
        return catEmpresaId;
    }

    public void setCatEmpresaId(CatEmpresa catEmpresaId) {
        this.catEmpresaId = catEmpresaId;
    }

    public String getClvTipoMovimiento() {
        return clvTipoMovimiento;
    }

    public void setClvTipoMovimiento(String clvTipoMovimiento) {
        this.clvTipoMovimiento = clvTipoMovimiento;
    }

    public String getClvMovimiento() {
        return clvMovimiento;
    }

    public void setClvMovimiento(String clvMovimiento) {
        this.clvMovimiento = clvMovimiento;
    }

    public String getNomOrigen() {
        return nomOrigen;
    }

    public void setNomOrigen(String nomOrigen) {
        this.nomOrigen = nomOrigen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}