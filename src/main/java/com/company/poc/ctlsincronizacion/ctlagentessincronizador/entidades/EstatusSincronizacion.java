package com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades;

import org.springframework.transaction.annotation.Transactional;

public enum EstatusSincronizacion {
    Pendiente("Pendiente"),
    Extraccion_Completada("Extracción Completada"),
    Instancia_Iniciada("Control de Instancias Iniciada"),
    Instancia_Completada("Control de Instancias Completada"),
    Transformacion_Iniciada("Transformación Iniciada"),
    Transformacion_Completada("Transformación Completada"),
    Transformacion_Enviadas("Transacciones Enviadas"),
    Error_Extraccion("Error Extracción");
    private String descripcion;

    EstatusSincronizacion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion(){
        return this.descripcion;
    }
}
