package com.company.poc.ctlsincronizacion.ctlagentessincronizador.reposotorios;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;

import java.util.List;

public interface CtlAgenteSincronizadorRepositorio {
    CtlAgenteSincronizador obtenerCtlAgenteSincronizadorPorId(Long id);

    List<CtlAgenteSincronizador> listarCtlAgenteSincronizadores();

    List<CtlAgenteSincronizador> listarCtlAgenteSincronizadoresParaEjecucion(String nombreEmpresa);

    void registrarInstancia(CtlAgenteSincronizador ctlAgenteSincronizador);
}
