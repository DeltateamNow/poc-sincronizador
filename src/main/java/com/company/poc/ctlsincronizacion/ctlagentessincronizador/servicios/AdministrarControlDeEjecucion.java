package com.company.poc.ctlsincronizacion.ctlagentessincronizador.servicios;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.EstatusSincronizacion;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.reposotorios.CtlAgenteSincronizadorRepositorio;
import com.company.poc.persistencia.fabricarepositorio.FabricaRepositorioPorImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AdministrarControlDeEjecucion {
    private final FabricaRepositorioPorImpl repositorioPorImpl;
    private CtlAgenteSincronizadorRepositorio ctlAgenteSincronizadorRepositorio;

    public AdministrarControlDeEjecucion(FabricaRepositorioPorImpl repositorioPorImpl) {
        this.repositorioPorImpl = repositorioPorImpl;
    }

    public List<CtlAgenteSincronizador> obtenerAgenteSincronizadorParaEjecucion(String nombreEmpresa) {
        return ctlAgenteSincronizadorRepositorio.listarCtlAgenteSincronizadoresParaEjecucion(nombreEmpresa);
    }

    public void registrarInstanciaIniciada(CtlAgenteSincronizador ctlAgenteSincronizador) {
        ctlAgenteSincronizador.setOpcEstatusSincronizacion(EstatusSincronizacion.Instancia_Iniciada.getDescripcion());
        ctlAgenteSincronizador.setFecControl(LocalDateTime.now());
        registroInstancia(ctlAgenteSincronizador);
    }

    public void registrarInstanciaCompleta(CtlAgenteSincronizador ctlAgenteSincronizador) {
        ctlAgenteSincronizador.setOpcEstatusSincronizacion(EstatusSincronizacion.Instancia_Completada.getDescripcion());
        ctlAgenteSincronizador.setFecControl(LocalDateTime.now());
        ctlAgenteSincronizador.setFecFinSincronizacion(LocalDateTime.now());
        registroInstancia(ctlAgenteSincronizador);
    }

    private void registroInstancia(CtlAgenteSincronizador ctlAgenteSincronizador) {
        ctlAgenteSincronizadorRepositorio.registrarInstancia(ctlAgenteSincronizador);
    }

    public CtlAgenteSincronizadorRepositorio getCtlAgenteSincronizadorRepositorio() {
        return ctlAgenteSincronizadorRepositorio;
    }

    public void setCtlAgenteSincronizadorRepositorio(CtlAgenteSincronizadorRepositorio ctlAgenteSincronizadorRepositorio) {
        this.ctlAgenteSincronizadorRepositorio = ctlAgenteSincronizadorRepositorio;
    }
}
