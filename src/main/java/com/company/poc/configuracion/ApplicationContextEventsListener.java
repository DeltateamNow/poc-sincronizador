package com.company.poc.configuracion;

import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextEventsListener {

    @EventListener({ContextStartedEvent.class})
    public void processContextStartedEvent() {
           //Inicializar los servicios y Entidades con
           // el Repositorio de consumo
    }
}
