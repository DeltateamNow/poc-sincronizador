/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.configuracion.procesocalculo;

import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author alvaro.ruiz
 */
@Component
@Order(2)
public class ContextoDeAplicacion {
    private Map<String, Object> mapPOC;

    public ContextoDeAplicacion(@Qualifier(value = "mapPOC")Map<String, Object> mapPOC, ApplicationContext ctxt) {
        this.setMapPOC((Map)mapPOC.get("mapPOC"));
        inicializaBeansAcosumirContext(ctxt);
    }

    private void inicializaBeansAcosumirContext(ApplicationContext context) {
        RegistroBeansDeCalculo registroBeansDeCalculo = (RegistroBeansDeCalculo) mapPOC.get("beansDeCalculo");
        registroBeansDeCalculo.getBeanUtilizadosEnCalculos().stream().forEach(name ->
                getMapPOC().put(name, context.getBean(name)));
    }

    public Map<String, Object> getMapPOC() {
        return mapPOC;
    }

    public void setMapPOC(Map<String, Object> mapPOC) {
        this.mapPOC = mapPOC;
    }
}
