package com.company.poc.configuracion.procesocalculo;

import java.util.List;

public class RegistroBeansDeCalculo {

    private List<String> beanUtilizadosEnCalculos;

    public List<String> getBeanUtilizadosEnCalculos() {
        return beanUtilizadosEnCalculos;
    }

    public void setBeanUtilizadosEnCalculos(List<String> beanUtilizadosEnCalculos) {
        this.beanUtilizadosEnCalculos = beanUtilizadosEnCalculos;
    }
}
