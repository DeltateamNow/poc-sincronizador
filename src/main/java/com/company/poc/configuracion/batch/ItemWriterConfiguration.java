package com.company.poc.configuracion.batch;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.servicios.AdministrarControlDeEjecucion;
import com.company.poc.ctlsincronizacion.ctlentrega.servicio.AlmacenarTransaccionesCalculadas;
import com.company.poc.dominio.entity.ContractHistory;
import com.company.poc.persistencia.fabricarepositorio.FabricaRepositorioPorImpl;
import com.company.poc.persistencia.jpaimpl.dominio.ContractHistoryJPARepo;
import com.company.poc.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Configuration
@Slf4j
public class ItemWriterConfiguration {

    private final ContractHistoryJPARepo contractHistoryJPARepo;
    private final Map<String, Object> mapPOC;
    private final AlmacenarTransaccionesCalculadas transaccionesCalculadas;
    private final  AtomicInteger count = new AtomicInteger();
    private final FabricaRepositorioPorImpl fabricaRepositorioPor;
    private final AdministrarControlDeEjecucion administrarControlDeEjecucion;

    public ItemWriterConfiguration(ContractHistoryJPARepo contractHistoryJPARepo,
                                   @Qualifier(value = "mapPOC") Map<String, Object> mapPOC,
                                   AlmacenarTransaccionesCalculadas transaccionesCalculadas,
                                   FabricaRepositorioPorImpl fabricaRepositorioPor,
                                   AdministrarControlDeEjecucion administrarControlDeEjecucion) {
        this.contractHistoryJPARepo = contractHistoryJPARepo;
        this.mapPOC = mapPOC;
        this.transaccionesCalculadas = transaccionesCalculadas;
        this.fabricaRepositorioPor = fabricaRepositorioPor;
        this.administrarControlDeEjecucion = administrarControlDeEjecucion;

        this.administrarControlDeEjecucion.setCtlAgenteSincronizadorRepositorio(
                fabricaRepositorioPor.obtenerInstanciaAgenteSincronizador(
                        FabricaRepositorioPorImpl.TipoRepositorio.JDBC_TEMPLATE));
        this.transaccionesCalculadas
                .setAdministrarControlDeEjecucion(this.administrarControlDeEjecucion);
    }

    @Bean
    @StepScope
    public ItemWriter<String> itemWriter(@Value("#{jobParameters['tabla']}") String tabla,
                                         @Value("#{jobParameters['agenteId']}") Long agenteId) {

        ItemWriter<String> itemWriter = new ItemWriter<String>() {
            @Override
            public void write(List<? extends String> items) throws Exception {
                //controlDeEntrega.agregarEntrega();
                transaccionesCalculadas.addItemsAContenedor(items, tabla, agenteId);
                log.info("item processed ->" + (count.addAndGet(items.size())));
                log.info("example processed ->" + items.get(0));
            }

        };
        return itemWriter;
    }
    @Transactional
    public void actualizaDB(NamedParameterJdbcTemplate jdbcTemplate, List<? extends String> items, String tabla) {

        List<ContractHistory> itemsContract = obtenerContractHistoryInstancias(items);
        SqlParameterSource[] sqlParameterSource = new SqlParameterSource[itemsContract.size()];
        int ind = 0;
        for (ContractHistory contractHistory : itemsContract) {
            sqlParameterSource[ind++] = new BeanPropertySqlParameterSource(contractHistory);
        }
        final String INSERT_QUERY = "INSERT INTO ContractHistory (contractId, amount, creationDate, duration," +
                "holderName, status) VALUES ( :contractId, :amount, :creationDate, :duration, :holderName, 'EFFECTIVE')";
        jdbcTemplate.batchUpdate(INSERT_QUERY, sqlParameterSource);

        List<String> contractIdList = itemsContract.stream().map(ch -> ch.getContractId()).collect(Collectors.toList());
        final String DELETE_QUERY = "DELETE FROM " + tabla + " WHERE contractId IN (:contractId)";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("contractId", contractIdList);
        jdbcTemplate.update(DELETE_QUERY, parameterSource);
        log.info("example processed ->"+items.size() +"registros ---->" );

    }

    private  List<ContractHistory> obtenerContractHistoryInstancias(List<? extends String> items) {
        return items.stream().map(msj -> msj.split(Util.SEPARADOR_MENSAJES)[0])
                .map(Util::getInstanceFromJson)
                .collect(Collectors.toList());
    }
}
