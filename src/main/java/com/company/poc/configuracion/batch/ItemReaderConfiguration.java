package com.company.poc.configuracion.batch;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.database.support.SqlServerPagingQueryProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ItemReaderConfiguration {
    @Bean
    @StepScope
    public ItemReader<Map<String, Object>> itemReaderMap(@Value("#{jobParameters['tabla']}") String tabla, ApplicationContext context,
                                                         DataSource dataSource) {

        System.out.println("Obtenemos un parametro insertado en el JobParameters para escribirlo" + tabla);
       // Map mapaProductos = (Map)context.getBean("mapProductos");
       // ProductoBean bean = mapaProductos.get(producto);

        JdbcPagingItemReader<Map<String, Object>> jdbcPagingItemReader = new JdbcPagingItemReader<>();
        jdbcPagingItemReader.setDataSource(dataSource);
        jdbcPagingItemReader.setPageSize(20000);

        PagingQueryProvider queryProvider = createQuery(tabla);
        jdbcPagingItemReader.setQueryProvider(queryProvider);

        jdbcPagingItemReader.setRowMapper((rs, i) -> {
            int numeroDeColumnas = rs.getMetaData().getColumnCount();
            Map<String, Object> mapRet = new HashMap<>();
            for(int indx = 1; indx <= numeroDeColumnas; indx++ ){
                mapRet.put(rs.getMetaData().getColumnName(indx), rs.getObject(indx));
            }
            return mapRet;
        });
        return jdbcPagingItemReader;
    }

    private PagingQueryProvider createQuery(String tabla) {
        // Este provider deberá de hacer de acuerdo a la plataforma de la Base de Datos
        //MySqlPagingQueryProvider query = new MySqlPagingQueryProvider();
        SqlServerPagingQueryProvider query = new SqlServerPagingQueryProvider();
        query.setSelectClause("Select *");
        // Solo en caso de SQL Server, dejar parametrizable el hint
        query.setFromClause("from " + tabla + "(NOLOCK)");

        query.setSortKeys(sortByCreationDate());
        return query;
    }

    private Map<String, Order> sortByCreationDate() {
        Map<String, Order> stringOrderMap = new HashMap<>();
        stringOrderMap.put("creationDate", Order.ASCENDING);
        return stringOrderMap;
    }

}
