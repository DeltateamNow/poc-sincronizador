package com.company.poc.configuracion.batch.listeners;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.EstatusSincronizacion;
import com.company.poc.ctlsincronizacion.ctlentrega.servicio.AlmacenarTransaccionesCalculadas;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.repositorios.CtlSincronizacionDiarioRepositorio;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.servicios.ControlDiarioSincronizacionServicio;
import com.company.poc.persistencia.fabricarepositorio.FabricaRepositorioPorImpl;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.web.client.RestTemplate;


public class JobExecutionListenerImpl implements JobExecutionListener {
    private final ControlDiarioSincronizacionServicio controlDiarioSincronizacionServicio;
    private final FabricaRepositorioPorImpl fabricaRepositorioPorimpl;
    private final AlmacenarTransaccionesCalculadas transaccionesCalculadas;

    public JobExecutionListenerImpl(ControlDiarioSincronizacionServicio controlDiarioSincronizacionServicio,
                                    FabricaRepositorioPorImpl fabricaRepositorioPorimpl,
                                    AlmacenarTransaccionesCalculadas transaccionesCalculadas) {
        this.controlDiarioSincronizacionServicio = controlDiarioSincronizacionServicio;
        this.fabricaRepositorioPorimpl = fabricaRepositorioPorimpl;
        this.transaccionesCalculadas = transaccionesCalculadas;
    }


    //parámetro inyectado en tiempo de ejecución, por dependencia de módulo


    @Override
    public void beforeJob(JobExecution jobExecution) {
        String tabla = jobExecution.getJobParameters().getString("tabla");
       transaccionesCalculadas.inicializaContendor(tabla);
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        // Se podrá utilizar para enterarse de que el job se ha terminado

        JobParameters jobParameters = jobExecution.getJobParameters();
        System.out.println("After Job" + jobExecution.getJobParameters());


        CtlSincronizacionDiarioRepositorio ctlSincronizacionDiarioRepositorio =
                fabricaRepositorioPorimpl.obtenerInstanciaSincronizacionDiario(
                        FabricaRepositorioPorImpl.TipoRepositorio.JDBC_TEMPLATE);
        controlDiarioSincronizacionServicio.setCtlSincronizacionDiarioRepositorio(ctlSincronizacionDiarioRepositorio);

        String tabla = jobParameters.getString("tabla");
        Long agenteId = jobParameters.getLong("agenteId");

        transaccionesCalculadas.ejecutaPersistencia(tabla, agenteId);
        transaccionesCalculadas.terminaContendor(tabla);
        // Mediante el query que se use para obtener los datos se requiere, realizar una consulta
        // Validar que el origen ya no tiene datos a procesar, en caso contrario, deberá de iniciar un nuevo Job con los mismos
        //parámetros haste que los datos origen sean cero
        if (controlDiarioSincronizacionServicio.tablaOrigenConRegistros(jobParameters)) {
            RestTemplate restTemplate = new RestTemplate();
            try {
                String url = "http://localhost:8081/reiniciar?tabla=" +
                        jobParameters.getString("tabla") + "&mapaCalculo=" +
                        jobParameters.getString("mapaCalculo");
                restTemplate.getForEntity(url, String.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else { //Revisar cuantos reintentos deberemos de pasar !!
            // Validar si el proceso se realiza con éxito o no
            controlDiarioSincronizacionServicio.actualizarAgenteCompletado(jobExecution, EstatusSincronizacion.Instancia_Completada);
        }
        System.out.println("After Job" + jobExecution.getJobId());
    }

}
