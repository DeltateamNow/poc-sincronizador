package com.company.poc.configuracion.batch;

import com.company.poc.configuracion.batch.listeners.JobExecutionListenerImpl;
import com.company.poc.ctlsincronizacion.ctlentrega.servicio.AlmacenarTransaccionesCalculadas;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.servicios.ControlDiarioSincronizacionServicio;
import com.company.poc.persistencia.fabricarepositorio.FabricaRepositorioPorImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
public class BatchConfigure extends DefaultBatchConfigurer {
    @Override
    @Bean
    public JobLauncher getJobLauncher() {
        try {
            SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
            SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
            jobLauncher.setJobRepository(getJobRepository());
            jobLauncher.setTaskExecutor(simpleAsyncTaskExecutor);
            jobLauncher.afterPropertiesSet();
            return jobLauncher;
        } catch (Exception e) {
            //log.error("Can't load SimpleJobLauncher with SimpleAsyncTaskExecutor: {} fallback on default", e);
            return super.getJobLauncher();
        }
    }

    @Bean
    public Job startBatch(JobBuilderFactory jobBuilderFactory, Step step,
                          ControlDiarioSincronizacionServicio controlDiarioSincronizacionServicio,
                          FabricaRepositorioPorImpl fabricaRepositorioPorimpl,
                          AlmacenarTransaccionesCalculadas transaccionesCalculadas) {

        JobExecutionListenerImpl listener = new JobExecutionListenerImpl(controlDiarioSincronizacionServicio,
                fabricaRepositorioPorimpl, transaccionesCalculadas);

        return jobBuilderFactory.get("contractEffective")
                .start(step)
                .listener(listener)
                .build();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        return new SimpleAsyncTaskExecutor("spring_batch");
    }

    @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory,
                      ItemReader<Map<String, Object>> itemReader,
                      ItemProcessor<Map<String, Object>, String> itemProcessor,
                      ItemWriter<String> itemWriter, TaskExecutor taskExecutor
    ) {
        return stepBuilderFactory.get("step1")
                .<Map<String, Object>, String>chunk(50)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .taskExecutor(taskExecutor)
                .throttleLimit(30)
                //.exceptionHandler()
                .build();

    }

    @Bean
    @Qualifier(value = "contextMap")
    // Se puede Crear un mapa de contextos por cada producto disponible
    // Se puede ubicar en otra clase dedicada a verificar la configuración de los productos
    // tal que cuando se ejecute el job se pase como parámetro el nombre del producto
    // en el reader podemos recuperar el contexto completo de dicho producto
    public Map<String, Object> contextMap(ApplicationContext context) {
        Map<String, Object> contextMap = new HashMap<>();
        contextMap.put("ctx1", context);
        return contextMap;
    }
}
