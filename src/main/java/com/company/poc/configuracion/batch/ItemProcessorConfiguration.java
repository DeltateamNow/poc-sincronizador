package com.company.poc.configuracion.batch;

import com.company.poc.configuracion.procesocalculo.ContextoDeAplicacion;
import com.company.poc.procesocalculo.MapaDeCalculo;
import com.company.poc.util.Util;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Configuration
@Slf4j
public class ItemProcessorConfiguration {
    private AtomicInteger count = new AtomicInteger();
    private final ContextoDeAplicacion contextoDeAplicacion;

    public ItemProcessorConfiguration(ContextoDeAplicacion contextoDeAplicacion){
        this.contextoDeAplicacion = contextoDeAplicacion;

    }

    @Bean
    @StepScope
    public ItemProcessor<Map<String, Object>, String>  itemProcessorMap(@Value("#{jobParameters['mapaCalculo']}") String mapaCalculo){
        final MapaDeCalculo mapaDeCalculo = (MapaDeCalculo) contextoDeAplicacion.getMapPOC().get(mapaCalculo);
        return map -> {
            //log.info("processing the data "+ map + " Record No : " + count.incrementAndGet());

            List<Map<String, Object>> respuesta = mapaDeCalculo.aplicarMapaDeDatos(map, contextoDeAplicacion.getMapPOC());

            String jsonStringList = respuesta.stream().map(mapa -> {
                    return Util.getJsonDeInstancia(map);
            }).collect(Collectors.joining(Util.SEPARADOR_MENSAJES));
            return jsonStringList;
        };
    }
}
