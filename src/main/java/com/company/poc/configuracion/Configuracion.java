/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.configuracion;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author alvaro.ruiz
 */
@Configuration
public class Configuracion {

    @Value("${path.archivo.configuracion}")
    private String pathArchivoConfiguracion;

    @Bean(name = "mapPOC")
    @Order(1)
    public Map<String, Object> getBeansMapPOC(ApplicationContext ctxt) {
        ApplicationContext poc = new FileSystemXmlApplicationContext(pathArchivoConfiguracion);
        Map<String, Object> beansMapPOC = new HashMap<>();
        Arrays.stream(poc.getBeanDefinitionNames())
                .filter(name -> !name.contains("."))
                .forEach(name
                        -> beansMapPOC.put(name, poc.getBean(name))
                );
        return beansMapPOC;
    }
}
