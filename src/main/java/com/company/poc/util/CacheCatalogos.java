package com.company.poc.util;

public enum CacheCatalogos {
    CatCanal,
    CatEmpresa,
    CtlAgenteSincronizador,
    CatProducto,
    CatCartera

}
