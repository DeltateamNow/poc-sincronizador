package com.company.poc.util;

import com.company.poc.dominio.entity.ContractHistory;
import com.company.poc.dominio.entity.ContractId;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Util {
    public final static String SEPARADOR_MENSAJES="¬";
    private Util(){}

    public static void imprimeEnConsola(List<Map<String, Object>> respuesta, ObjectMapper mapper) {
        for (Map<String, Object> record : respuesta) {
            String jsonString = null;
            try {
                jsonString = mapper.writeValueAsString(record);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(jsonString);
                System.out.println(jsonObj.toString(2));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public static ContractHistory getInstanceFromJson(String jsonContractHistory) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        ContractHistory resultHistory = null;
        try {
            resultHistory = mapper.readValue(jsonContractHistory, ContractHistory.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return resultHistory;
    }


    public static ContractId getInstanceIdFromJson(String jsonContractId) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        ContractId resultId = null;
        try {
            resultId = mapper.readValue(jsonContractId, ContractId.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return resultId;

    }

    public static <T> Stream<List<T>> chunked(Stream<T> stream, int chunkSize) {
        AtomicInteger index = new AtomicInteger(0);
        return stream.collect(Collectors.groupingBy(x -> index.getAndIncrement() / chunkSize))
                .entrySet().stream()
                .map(Map.Entry::getValue);
    }

    public static <T> String getJsonDeInstancia(T instancia){
        String jsonString = null;
        try {
            jsonString =  objectMapper().writeValueAsString(instancia);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    public static ObjectMapper objectMapper() {
        ObjectMapper objectMapperObj = new ObjectMapper();
        objectMapperObj.findAndRegisterModules();
        objectMapperObj.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS");
        df.setTimeZone(TimeZone.getDefault());
        objectMapperObj.setDateFormat(df);

        return objectMapperObj;
    }
}
