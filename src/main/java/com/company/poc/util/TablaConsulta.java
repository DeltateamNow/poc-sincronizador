package com.company.poc.util;

import java.util.Arrays;

public enum TablaConsulta {
    Contract("Contract"),
    Contract2("Contract2"),
    ;

    private final String nomTabla;

    TablaConsulta(String nomTabla){
        this.nomTabla = nomTabla;
    }

    public String getNomTabla() {
        return nomTabla;
    }

    public static String getNombreTabla(String tabla) {
       return Arrays.stream(TablaConsulta.values())
                .filter(tbl -> tbl.getNomTabla().equals(tabla))
                .findAny().orElseThrow(RuntimeException::new).getNomTabla();
    }
}
