package com.company.poc.aplicativo;

import com.company.poc.configuracion.batch.listeners.JobExecutionListenerImpl;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.servicios.AdministrarControlDeEjecucion;
import com.company.poc.ctlsincronizacion.ctlentrega.servicio.AlmacenarTransaccionesCalculadas;
import com.company.poc.persistencia.fabricarepositorio.FabricaRepositorioPorImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class ProcesarJobsMapeosCalculo {

    private final JobLauncher jobLauncher;
    private final Job job;
    private final AdministrarControlDeEjecucion administrarControlDeEjecucion;
    private final FabricaRepositorioPorImpl fabricaRepositorioPor;

    public ProcesarJobsMapeosCalculo(JobLauncher jobLauncher, Job job,
                                     AlmacenarTransaccionesCalculadas transaccionesCalculadas,
                                     AdministrarControlDeEjecucion administrarControlDeEjecucion,
                                     FabricaRepositorioPorImpl fabricaRepositorioPor) {
        this.jobLauncher = jobLauncher;
        this.job = job;
        this.administrarControlDeEjecucion = administrarControlDeEjecucion;
        this.fabricaRepositorioPor = fabricaRepositorioPor;
    }

    public List<Long> ejecutarJobs(List<CtlAgenteSincronizador> agentes) {
        // inicializar listener con instancia de control diario
        List<Long> idsJobs = null;
        try {

            JobParameters jobParameters = new JobParametersBuilder()
                    .addLong("time", System.currentTimeMillis())
                    .addString("tabla", "Contract")
                    .addString("mapaCalculo", "mapa2ContractHistory")
                    .addLong("agenteId", 1l)
                    .toJobParameters();

            JobParameters jobParameters2 = new JobParametersBuilder()
                    .addLong("time", System.currentTimeMillis())
                    .addString("tabla", "Contract2")
                    .addString("mapaCalculo", "mapa2ContractHistory")
                    .toJobParameters();

            JobExecution execution = jobLauncher.run(job, jobParameters);
            JobExecution execution2 = jobLauncher.run(job, jobParameters2);

            idsJobs = Arrays.asList(execution.getJobId(), execution2.getJobId());

            agentes.stream().forEach(agente -> {
                // Realizar el Job de acuerdo al detalle del agente
                // Colocar el id del Agente que se utilizó para la ejecución, requerido para re-ejecutar proceso
                registrarInstanciaIniciada(jobParameters, agente);
            });


        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }
        return idsJobs;
    }

    private void registrarInstanciaIniciada(JobParameters jobParameters, CtlAgenteSincronizador agentes) {
        administrarControlDeEjecucion.setCtlAgenteSincronizadorRepositorio(
                fabricaRepositorioPor.obtenerInstanciaAgenteSincronizador(
                        FabricaRepositorioPorImpl.TipoRepositorio.JDBC_TEMPLATE));
        administrarControlDeEjecucion.registrarInstanciaIniciada(agentes);
    }
}