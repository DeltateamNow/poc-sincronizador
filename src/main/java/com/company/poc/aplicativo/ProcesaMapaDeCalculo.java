/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.aplicativo;

import com.company.poc.configuracion.procesocalculo.ContextoDeAplicacion;
import com.company.poc.procesocalculo.MapaDeCalculo;
import com.company.poc.procesocalculo.servicios.IdentificacionCanalServicio;
import com.company.poc.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.core.annotation.Order;

/**
 * @author alvaro.ruiz
 */
@Component
@Order(3)
public class ProcesaMapaDeCalculo {

    public void ejecuta(ContextoDeAplicacion contextoDeAplicacion) {
        LocalTime init = LocalTime.now();
        int value = 0;
        for (int i = 0; i < 10; i++) {
            Map<String, Object> entradaPOC = new HashMap<>();
            entradaPOC.put("entrada1", "Hello" + i);
            entradaPOC.put("entrada2", i);
            entradaPOC.put("entrada3", 26 + i);
            if (i % 2 == 0) {
                entradaPOC.put("entrada4", "Laredo");
            } else {
                entradaPOC.put("entrada4", "Mexico");
            }
            MapaDeCalculo mapaDeCalculo = (MapaDeCalculo) contextoDeAplicacion.getMapPOC().get("mapaDoble");
            List<Map<String, Object>> respuesta = mapaDeCalculo.aplicarMapaDeDatos(entradaPOC, contextoDeAplicacion.getMapPOC());
            ObjectMapper mapper = new ObjectMapper();
            Util.imprimeEnConsola(respuesta, mapper);
            IdentificacionCanalServicio identificacionCanal = (IdentificacionCanalServicio) contextoDeAplicacion.getMapPOC().get("identificacionCanal");
            identificacionCanal.identificacionCanal(1220, -1);
            value = i;
        }
        System.out.println("Init --->" + init);
        System.out.println("value --->" + value);
        System.out.println("End --->" + LocalTime.now());
    }
}
