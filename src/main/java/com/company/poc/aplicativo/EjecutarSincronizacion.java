package com.company.poc.aplicativo;


import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.reposotorios.CtlAgenteSincronizadorRepositorio;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.servicios.AdministrarControlDeEjecucion;
import com.company.poc.persistencia.fabricarepositorio.FabricaRepositorioPorImpl;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Service
public class EjecutarSincronizacion {

    private final AdministrarControlDeEjecucion administrarControlDeEjecucion;
    private final ProcesarJobsMapeosCalculo procesarJobsMapeosCalculo;
    private final  JobLauncher jobLauncher;
    private final  Job job;
    private final FabricaRepositorioPorImpl fabricaRepositorioPorImpl;
    public EjecutarSincronizacion(AdministrarControlDeEjecucion administrarControlDeEjecucion,
                                  ProcesarJobsMapeosCalculo procesarJobsMapeosCalculo,
                                  JobLauncher jobLauncher, Job job, FabricaRepositorioPorImpl fabricaRepositorioPor) {
        this.administrarControlDeEjecucion = administrarControlDeEjecucion;
        this.procesarJobsMapeosCalculo = procesarJobsMapeosCalculo;
        this.jobLauncher = jobLauncher;
        this.job = job;
        this.fabricaRepositorioPorImpl = fabricaRepositorioPor;
    }

    public List<Long> procesarSincronizacion(String nombreEmpresa){
        administrarControlDeEjecucion.setCtlAgenteSincronizadorRepositorio(
                fabricaRepositorioPorImpl.obtenerInstanciaAgenteSincronizador(FabricaRepositorioPorImpl.TipoRepositorio.JPA));
        List<CtlAgenteSincronizador> agentes = administrarControlDeEjecucion.obtenerAgenteSincronizadorParaEjecucion(nombreEmpresa);

        return procesarJobsMapeosCalculo.ejecutarJobs(agentes);
    }
    public String reIniciarSincronizacion(@RequestParam String tabla, @RequestParam String mapaCalculo) {
        CtlAgenteSincronizadorRepositorio repositorio = fabricaRepositorioPorImpl
                .obtenerInstanciaAgenteSincronizador(FabricaRepositorioPorImpl.TipoRepositorio.JPA);
        /*
           Obtener el Id de los parámetros de Job y pasarlo como atributo
           Requerido para actualizar el agente sincronizador como estancia iniciada
         */
        //repositorio.obtenerCtlAgenteSincronizadorPorId()

        String result = "Fallido";
        JobParameters jobParameters = new JobParametersBuilder()
                .addLong("time", System.currentTimeMillis())
                .addString("tabla", tabla)
                .addString("mapaCalculo", mapaCalculo)
                .toJobParameters();
                result = "OK";
        try {
            JobExecution execution = jobLauncher.run(job, jobParameters);
        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }
        return result;
    }
}
