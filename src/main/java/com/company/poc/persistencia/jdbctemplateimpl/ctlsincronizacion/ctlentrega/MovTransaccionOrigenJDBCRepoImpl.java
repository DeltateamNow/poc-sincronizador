package com.company.poc.persistencia.jdbctemplateimpl.ctlsincronizacion.ctlentrega;

import com.company.poc.ctlsincronizacion.ctlentrega.repositorios.MovTransaccionOrigenRepositorio;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovTransaccionOrigenJDBCRepoImpl implements MovTransaccionOrigenRepositorio {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    public MovTransaccionOrigenJDBCRepoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    @Override
    public void removerRegistroOrigen(Long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("Id", id);
        jdbcTemplate.update("Deleted from MovTransaccionOrigen where Id :=Id",map);
    }

    @Override
    public void removerRegistrosOrigen(List<Long> ids) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("Id", ids);
        jdbcTemplate.update("Deleted from MovTransaccionOrigen where Id :=Id",parameterSource);
    }
}
