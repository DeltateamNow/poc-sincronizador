package com.company.poc.persistencia.jdbctemplateimpl.ctlsincronizacion.ctlentrega;

import com.company.poc.ctlsincronizacion.ctlentrega.entidades.MovTransaccionOmnicanal;
import com.company.poc.ctlsincronizacion.ctlentrega.repositorios.MovTransaccionOmnicanalRepositorio;
import com.company.poc.util.Util;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
@Repository
@Qualifier("MovTransaccionOmnicanalJDBCRepoImpl")
public class MovTransaccionOmnicanalJDBCRepoImpl implements MovTransaccionOmnicanalRepositorio {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    public MovTransaccionOmnicanalJDBCRepoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    @Override
    public void registrarTransaccionOmnicanal(MovTransaccionOmnicanal movTransaccionOmnicanal) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(this.dataSource)
                .withTableName("MovTransaccionOmnicanal");
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(movTransaccionOmnicanal);
        insert.execute(parameterSource);
    }

    @Override
    public void registrarTransaccionesOmnicanal(List<MovTransaccionOmnicanal> movTransaccionesOmnicanal) {
        if(movTransaccionesOmnicanal.size() > 2000){
            Util.chunked(movTransaccionesOmnicanal.stream(),1500)
                    .forEach(chunk -> registrarTransaccionesOmnicanal(chunk));
        }
        SimpleJdbcInsert insert = new SimpleJdbcInsert(this.dataSource)
                .withTableName("MovTransaccionOmnicanal");
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(movTransaccionesOmnicanal);
        insert.executeBatch(parameterSource);

    }


}
