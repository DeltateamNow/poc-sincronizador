package com.company.poc.persistencia.jdbctemplateimpl.ctlsincronizacion.ctlagentesincronizador;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.reposotorios.CtlAgenteSincronizadorRepositorio;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier("ctlAgenteSincronizadorJDBCTemplateRepoImpl")
public class CtlAgenteSincronizadorJDBCTemplateRepoImpl implements CtlAgenteSincronizadorRepositorio {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;
    private final RowMapper<CtlAgenteSincronizador> rowMapper;

    public CtlAgenteSincronizadorJDBCTemplateRepoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
        rowMapper = BeanPropertyRowMapper.newInstance(CtlAgenteSincronizador.class);
    }

    @Override
    @Cacheable(cacheNames = "CtlAgenteSincronizador", key = "#id")
    public CtlAgenteSincronizador obtenerCtlAgenteSincronizadorPorId(Long id) {
        Map<String, Object> param = new HashMap<>();
        param.put("Id",id);
        return jdbcTemplate.queryForObject("Select * from CtlAgenteSincronizador where Id =:Id ",param, rowMapper);
    }

    @Override
    public List<CtlAgenteSincronizador> listarCtlAgenteSincronizadores() {

        throw new UnsupportedOperationException();
    }

    @Override
    public List<CtlAgenteSincronizador> listarCtlAgenteSincronizadoresParaEjecucion(String nombreEmpresa) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void registrarInstancia(CtlAgenteSincronizador ctlAgenteSincronizador) {

        String query = "INSERT INTO CtlAgenteSincronizador \n" +
                " ( CtlConfiguracionSincronizadorId, CatEmpresaId, CatProductoId, NomAgente, NomOrigen, ClvMovimiento, " +
                "   ClvTipoMovimiento, OpcActivo, FecInicioSincronizacion, FecFinSincronizacion, " +
                "     FecControl, OpcEstatusSincronizacion) \n" +
                "  VALUES(:CtlConfiguracionSincronizadorId, :CatEmpresaId, :CatProductoId, :NomAgente, " +
                "  :NomOrigen, :ClvMovimiento, :ClvTipoMovimiento, :OpcActivo, :FecInicioSincronizacion, " +
                "  :FecFinSincronizacion, :FecControl , :OpcEstatusSincronizacion); \n";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("CtlConfiguracionSincronizadorId"
                , ctlAgenteSincronizador.getCtlConfiguracionSincronizadorId().getId());
        parameterSource.addValue("CatEmpresaId"
                , ctlAgenteSincronizador.getCatEmpresaId().getId());
        parameterSource.addValue("CatProductoId"
                , ctlAgenteSincronizador.getCatProductoId().getId());
        parameterSource.addValue("NomAgente", ctlAgenteSincronizador.getNomAgente());
        parameterSource.addValue("NomOrigen", ctlAgenteSincronizador.getNomOrigen());
        parameterSource.addValue("ClvMovimiento", ctlAgenteSincronizador.getClvMovimiento());
        parameterSource.addValue("ClvTipoMovimiento", ctlAgenteSincronizador.getClvTipoMovimiento());
        parameterSource.addValue("OpcActivo", ctlAgenteSincronizador.isOpcActivo()?1:0);
        parameterSource.addValue("FecInicioSincronizacion", ctlAgenteSincronizador.getFecInicioSincronizacion());
        parameterSource.addValue("FecFinSincronizacion", ctlAgenteSincronizador.getFecFinSincronizacion());
        parameterSource.addValue("FecControl", ctlAgenteSincronizador.getFecControl());
        parameterSource.addValue("OpcEstatusSincronizacion", ctlAgenteSincronizador.getOpcEstatusSincronizacion());

        jdbcTemplate.update(query, parameterSource);


    }
}
