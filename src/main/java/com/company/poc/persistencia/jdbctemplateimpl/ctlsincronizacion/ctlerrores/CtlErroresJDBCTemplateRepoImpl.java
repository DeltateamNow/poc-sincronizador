package com.company.poc.persistencia.jdbctemplateimpl.ctlsincronizacion.ctlerrores;

import com.company.poc.ctlsincronizacion.ctlerrores.entidades.CtlErroresSincronizacion;
import com.company.poc.ctlsincronizacion.ctlerrores.repositorios.CtlErroresRepositorio;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
@Qualifier("ctlErroresJDBCTemplateRepoImpl")
public class CtlErroresJDBCTemplateRepoImpl implements CtlErroresRepositorio {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    public CtlErroresJDBCTemplateRepoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    @Override
    public void registrarErrorSincronizacion(CtlErroresSincronizacion ctlErroresSincronizacion) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(this.dataSource)
                .withTableName("CtlErroresSincronizacion");
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(ctlErroresSincronizacion);
        insert.execute(parameterSource);
    }
}
