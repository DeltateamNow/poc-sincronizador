package com.company.poc.persistencia.jdbctemplateimpl.ctlsincronizacion.ctlsincronizaciondiaria;

import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.entidades.CtlSincronizacionesDiario;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.repositorios.CtlSincronizacionDiarioRepositorio;
import com.company.poc.util.TablaConsulta;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Map;

@Repository
@Qualifier("ctlSincronizacionDiarioJDBCRepoImpl")
public class CtlSincronizacionDiarioJDBCRepoImpl implements CtlSincronizacionDiarioRepositorio {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    public CtlSincronizacionDiarioJDBCRepoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    @Override
    public void registrarSincronizacionDiaria(CtlSincronizacionesDiario ctlSincronizacionDiarioRepositorio) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(this.dataSource)
                .withTableName("CtlSincronizacionesDiario");
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(ctlSincronizacionDiarioRepositorio);
        insert.execute(parameterSource);

    }

    @Override
    public boolean tablaEnEjecucionConDatos(String tabla) {
        String query = "Select count(*) from " + TablaConsulta.getNombreTabla(tabla);
        Integer count = jdbcTemplate.queryForObject(query, (Map<String, ?>) null, Integer.class);
        //log.info("tablaOrigenConRegistros ->" +  query + " count=" + count);
        return count > 0 ? true : false;
    }
}
