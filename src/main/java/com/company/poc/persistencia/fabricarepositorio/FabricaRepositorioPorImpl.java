package com.company.poc.persistencia.fabricarepositorio;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.reposotorios.CtlAgenteSincronizadorRepositorio;
import com.company.poc.ctlsincronizacion.ctlconfiguracioncatalogos.repositorios.CtlConfiguracionCatalogoRepositorio;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.repositorios.CtlSincronizacionDiarioRepositorio;
import com.company.poc.persistencia.jpaimpl.dominio.CatCarteraJPARepositorioImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FabricaRepositorioPorImpl {
    @Autowired
    @Qualifier("ctlAgenteSincronizadorJDBCTemplateRepoImpl")
    CtlAgenteSincronizadorRepositorio ctlAgenteSincronizadorRepositorioJDBC;
    @Autowired
    @Qualifier("ctlAgenteSincronizadorJPARepositorioImpl")
    CtlAgenteSincronizadorRepositorio ctlAgenteSincronizadorRepositorioJPA;
    @Autowired
    @Qualifier("ctlSincronizacionDiarioJDBCRepoImpl")
    CtlSincronizacionDiarioRepositorio ctlSincronizacionDiarioRepositorioJDBC;
    @Autowired
    @Qualifier("ctlSincronizacionDiarioJPARepositorioImpl")
    CtlSincronizacionDiarioRepositorio ctlSincronizacionDiarioRepositorioJPA;
    @Autowired
    @Qualifier("ctlConfiguracionCatalogoRepositorioJPAImpl")
    CtlConfiguracionCatalogoRepositorio ctlConfiguracionCatalogoRepositorioJPA;
    @Autowired
    CatCarteraJPARepositorioImpl catCarteraJPARepositorioImpl;


    public CtlAgenteSincronizadorRepositorio obtenerInstanciaAgenteSincronizador(TipoRepositorio tipoRepositorio) {
        CtlAgenteSincronizadorRepositorio repositorio = null;
        switch (tipoRepositorio) {
            case JDBC_TEMPLATE:
                repositorio = ctlAgenteSincronizadorRepositorioJDBC;
                break;
            case JPA:
                repositorio = ctlAgenteSincronizadorRepositorioJPA;
                break;
        }
        return repositorio;
    }

    public CtlSincronizacionDiarioRepositorio obtenerInstanciaSincronizacionDiario(TipoRepositorio tipoRepositorio) {
        CtlSincronizacionDiarioRepositorio repositorio = null;
        switch (tipoRepositorio) {
            case JDBC_TEMPLATE:
                repositorio = ctlSincronizacionDiarioRepositorioJDBC;
                break;
            case JPA:
                repositorio = ctlSincronizacionDiarioRepositorioJPA;
                break;
        }
        return repositorio;
    }

    public CtlConfiguracionCatalogoRepositorio obtenerInstanciaCtlConfiguracionCatalogo(TipoRepositorio tipoRepositorio) {
        CtlConfiguracionCatalogoRepositorio resultado = null;
        switch (tipoRepositorio) {

            case JDBC_TEMPLATE:
                break;
            case JPA:
                resultado = ctlConfiguracionCatalogoRepositorioJPA;
                break;
        }
        return resultado;
    }


    public static enum TipoRepositorio {
        JDBC_TEMPLATE,
        JPA
    }

    public static enum Catalogo {
        CatCanal,
        CatCartera
    }


}
