package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlagentessincronizador;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import com.company.poc.ctlsincronizacion.ctlagentessincronizador.reposotorios.CtlAgenteSincronizadorRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Qualifier("ctlAgenteSincronizadorJPARepositorioImpl")
public class CtlAgenteSincronizadorJPARepositorioImpl implements CtlAgenteSincronizadorRepositorio {
    @Autowired
    CtlAgenteSincronizadorJPARepo ctlAgenteSincronizadorJPARepo;

    @Override
    public CtlAgenteSincronizador obtenerCtlAgenteSincronizadorPorId(Long id) {
        CtlAgenteSincronizador resultado = null;
        Optional<CtlAgenteSincronizador> optionalCtlAgenteSincronizador = ctlAgenteSincronizadorJPARepo.findById(id);
        if (optionalCtlAgenteSincronizador.isPresent()) {
            resultado = optionalCtlAgenteSincronizador.get();
        }
        return resultado;
    }

    @Override
    public List<CtlAgenteSincronizador> listarCtlAgenteSincronizadores() {
        return ctlAgenteSincronizadorJPARepo.findAll();
    }

    @Override
    public List<CtlAgenteSincronizador> listarCtlAgenteSincronizadoresParaEjecucion(String nombreEmpresa) {
        return ctlAgenteSincronizadorJPARepo.encontrarAgentesParaEjecutar(nombreEmpresa);
    }

    @Override
    public void registrarInstancia(CtlAgenteSincronizador ctlAgenteSincronizador) {
        ctlAgenteSincronizadorJPARepo.save(ctlAgenteSincronizador);
    }


}
