package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatTipoTransaccion;
import com.company.poc.dominio.repositorio.CatalogoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatTipoTransaccionJPARepositorioImpl implements CatalogoRepositorio<CatTipoTransaccion, Long> {
    @Autowired
    private CatTipoTransaccionJPARepositorio catTipoTransaccionJPARepositorio;

    public CatTipoTransaccionJPARepositorioImpl() {
    }

    @Override
    @Cacheable(cacheNames = "CatTipoTransaccion")
    public List<CatTipoTransaccion> obtenerListadoCatalogo() {
        return catTipoTransaccionJPARepositorio.findAll();
    }

    @Override
    @Cacheable(cacheNames = "CatTipoTransaccion", key = "#id")
    public CatTipoTransaccion obtenerEntidadPorId(Long id) {
        return catTipoTransaccionJPARepositorio.findById(id).orElse(null);
    }
}
