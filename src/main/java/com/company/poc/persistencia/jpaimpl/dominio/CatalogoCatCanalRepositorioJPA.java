package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatCanal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogoCatCanalRepositorioJPA extends JpaRepository<CatCanal, Long> {

}
