package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlerrores;

import com.company.poc.ctlsincronizacion.ctlerrores.entidades.CtlErroresSincronizacion;
import com.company.poc.ctlsincronizacion.ctlerrores.repositorios.CtlErroresRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("ctlErroresJPARepoImpl")
public class CtlErroresJPARepoImpl implements CtlErroresRepositorio {
    @Autowired
    CtlErroresJPARepo ctlErroresJPARepo;

    @Override
    public void registrarErrorSincronizacion(CtlErroresSincronizacion ctlErroresSincronizacion) {
        ctlErroresJPARepo.save(ctlErroresSincronizacion);
    }
}
