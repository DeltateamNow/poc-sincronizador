package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlsincronizaciondiaria;

import com.company.poc.ctlsincronizacion.ctlentrega.entidades.MovTransaccionOmnicanal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovTransaccionOmnicanalJPARepo extends JpaRepository<MovTransaccionOmnicanal, Long> {
}
