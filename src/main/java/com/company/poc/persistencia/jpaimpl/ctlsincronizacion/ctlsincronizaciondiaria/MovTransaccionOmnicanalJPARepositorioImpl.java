package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlsincronizaciondiaria;

import com.company.poc.ctlsincronizacion.ctlentrega.entidades.MovTransaccionOmnicanal;
import com.company.poc.ctlsincronizacion.ctlentrega.repositorios.MovTransaccionOmnicanalRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Qualifier("movTransaccionOmnicanalJPARepositorioImpl")
public class MovTransaccionOmnicanalJPARepositorioImpl implements MovTransaccionOmnicanalRepositorio {
    @Autowired
    MovTransaccionOmnicanalJPARepo movTransaccionOmnicanalJPARepo;
    @Override
    public void registrarTransaccionOmnicanal(MovTransaccionOmnicanal movTransaccionOmnicanal) {
        movTransaccionOmnicanalJPARepo.save(movTransaccionOmnicanal);
    }

    @Override
    public void registrarTransaccionesOmnicanal(List<MovTransaccionOmnicanal> movTransaccionesOmnicanal) {
        movTransaccionOmnicanalJPARepo.saveAll(movTransaccionesOmnicanal);
    }
}
