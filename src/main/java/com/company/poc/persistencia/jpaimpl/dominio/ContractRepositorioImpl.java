package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.entity.Contract;
import com.company.poc.dominio.repositorio.ContractRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ContractRepositorioImpl  implements ContractRepositorio {
    @Autowired
    ContractJPARepo contractJPARepo;

    @Override
    public void saveAll(List<Contract> contractList) {
        contractJPARepo.saveAll(contractList);
    }

    @Override
    public void eliminarAll(List<Contract> contractList){
        contractJPARepo.deleteAll(contractList);
    }
}
