package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatEmpresaJPARepositorio extends JpaRepository<CatEmpresa, Long> {

}
