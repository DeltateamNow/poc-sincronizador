package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlagentessincronizador;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CtlAgenteSincronizadorJPARepo extends JpaRepository<CtlAgenteSincronizador, Long> {

    // Query nativo SQL Server
    @Query(value = "Select ca.* from CtlAgenteSincronizador ca inner join " +
            "   ( select CatEmpresaId ,NomAgente ,CatProductoId, NomOrigen, MAX(FecControl) as FecControl " +
            "       from CtlAgenteSincronizador  " +
            "       GROUP by CatEmpresaId, NomAgente, CatProductoId, NomOrigen " +
            "    ) as cltDerecha " +
            "   on cltDerecha.FecControl = ca.FecControl " +
            "   join CatEmpresa ce  on ce.Id = ca.CatEmpresaId and ce.NomEmpresa = :nomEmpresa " +
            "   where OpcEstatusSincronizacion = 'Extracción Completada' ", nativeQuery = true)
    List<CtlAgenteSincronizador> encontrarAgentesParaEjecutar(String nomEmpresa);

}
