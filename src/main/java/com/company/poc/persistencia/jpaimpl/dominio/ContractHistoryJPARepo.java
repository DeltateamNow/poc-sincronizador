package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.entity.ContractHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractHistoryJPARepo extends JpaRepository<ContractHistory, String> {
}

