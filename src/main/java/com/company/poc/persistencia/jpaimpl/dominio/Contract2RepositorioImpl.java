package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.entity.Contract2;
import com.company.poc.dominio.repositorio.Contract2Repositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class Contract2RepositorioImpl implements Contract2Repositorio {
    @Autowired
    Contract2JPARepo contract2JPARepo;
    @Override
    public void saveAll(List<Contract2> contractList2) {
        contract2JPARepo.saveAll(contractList2);
    }

    @Override
    public void eliminarAll(List<Contract2> contractList) {
        contract2JPARepo.deleteAll(contractList);
    }
}
