package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.entity.ContractHistory;
import com.company.poc.dominio.repositorio.ContractHistoryRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ContractHistoryRepositorioImpl implements ContractHistoryRepositorio {
    @Autowired
    ContractHistoryJPARepo contractHistoryJPARepo;

    @Override
    public void save(ContractHistory contractHistory) {
        contractHistoryJPARepo.save(contractHistory);
    }

    @Override
    public void saveAll(List<ContractHistory> contractList) {
        contractHistoryJPARepo.saveAll(contractList);
    }
}
