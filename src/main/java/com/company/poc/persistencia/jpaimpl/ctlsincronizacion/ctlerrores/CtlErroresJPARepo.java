package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlerrores;

import com.company.poc.ctlsincronizacion.ctlerrores.entidades.CtlErroresSincronizacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CtlErroresJPARepo extends JpaRepository<CtlErroresSincronizacion, Long> {
}
