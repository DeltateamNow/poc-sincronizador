package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatCartera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatCarteraJPARepositorio extends JpaRepository<CatCartera, Long> {
}
