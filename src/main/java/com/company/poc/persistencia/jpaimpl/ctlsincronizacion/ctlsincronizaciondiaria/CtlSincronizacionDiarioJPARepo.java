package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlsincronizaciondiaria;

import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.entidades.CtlSincronizacionesDiario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CtlSincronizacionDiarioJPARepo extends JpaRepository<CtlSincronizacionesDiario, Long> {
}
