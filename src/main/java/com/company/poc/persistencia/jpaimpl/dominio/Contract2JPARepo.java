package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.entity.Contract2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Contract2JPARepo extends  JpaRepository<Contract2, String>{
}
