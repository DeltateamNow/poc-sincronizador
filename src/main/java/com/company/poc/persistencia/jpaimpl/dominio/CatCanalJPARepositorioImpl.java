package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatCanal;
import com.company.poc.dominio.repositorio.CatalogoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatCanalJPARepositorioImpl implements CatalogoRepositorio<CatCanal, Long> {
    @Autowired
    CatalogoCatCanalRepositorioJPA catalogoCatCanalRepositorioJPA;

    @Override
    @Cacheable(cacheNames = "CatCanal")
    public List<CatCanal> obtenerListadoCatalogo() {
        return catalogoCatCanalRepositorioJPA.findAll();
    }

    @Override
    @Cacheable(cacheNames = "CatCanal", key = "#id")
    public CatCanal obtenerEntidadPorId(Long id) {
        return catalogoCatCanalRepositorioJPA.findById(id).orElse(null);
    }

}
