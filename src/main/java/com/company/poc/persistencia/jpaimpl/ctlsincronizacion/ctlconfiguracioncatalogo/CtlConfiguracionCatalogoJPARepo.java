package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlconfiguracioncatalogo;


import com.company.poc.ctlsincronizacion.ctlconfiguracioncatalogos.CtlConfiguracionCatalogo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
@Qualifier("CtlConfiguracionCatalogoJPARepo")
public interface CtlConfiguracionCatalogoJPARepo extends JpaRepository<CtlConfiguracionCatalogo, Long> {
    List<CtlConfiguracionCatalogo>
        findByFecUltimaActualizacionGreaterThanAndNomCatalogoDestino(LocalDateTime date, String catalogo);
}
