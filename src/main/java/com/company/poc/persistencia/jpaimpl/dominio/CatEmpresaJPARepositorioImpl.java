package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatEmpresa;
import com.company.poc.dominio.repositorio.CatalogoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatEmpresaJPARepositorioImpl implements CatalogoRepositorio<CatEmpresa, Long> {

    @Autowired
    private CatEmpresaJPARepositorio catEmpresaJPARepositorio;

    public CatEmpresaJPARepositorioImpl() {
    }

    @Override
    @Cacheable(cacheNames = "CatEmpresa")
    public List<CatEmpresa> obtenerListadoCatalogo() {
        return catEmpresaJPARepositorio.findAll();
    }

    @Override
    @Cacheable(cacheNames = "CatEmpresa", key = "#id")
    public CatEmpresa obtenerEntidadPorId(Long id) {
        return catEmpresaJPARepositorio.findById(id).orElse(null);
    }
}
