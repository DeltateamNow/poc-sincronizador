package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatCartera;
import com.company.poc.dominio.repositorio.CatalogoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatCarteraJPARepositorioImpl implements CatalogoRepositorio<CatCartera, Long> {
    @Autowired
    private CatCarteraJPARepositorio catCarteraJPARepositorio;

    public CatCarteraJPARepositorioImpl() {
    }

    @Override
    @Cacheable(cacheNames = "CatCartera")
    public List<CatCartera> obtenerListadoCatalogo() {
        return catCarteraJPARepositorio.findAll();
    }

    @Override
    @Cacheable(cacheNames = "CatCartera", key = "#id")
    public CatCartera obtenerEntidadPorId(Long id) {
        return catCarteraJPARepositorio.findById(id).orElse(null);
    }
}
