package com.company.poc.persistencia.jpaimpl.dominio;

import com.company.poc.dominio.catalogos.CatProducto;
import com.company.poc.dominio.repositorio.CatalogoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CatProductoJPARepositorioImpl implements CatalogoRepositorio<CatProducto, Long> {
    @Autowired
    private CatProductoJPARepositorio catProductoJPARepositorio;

    public CatProductoJPARepositorioImpl() {
    }

    @Override
    @Cacheable(cacheNames = "CatProducto")
    public List<CatProducto> obtenerListadoCatalogo() {
        return catProductoJPARepositorio.findAll();
    }

    @Override
    @Cacheable(cacheNames = "CatProducto", key = "#id")
    public CatProducto obtenerEntidadPorId(Long id) {
        return catProductoJPARepositorio.findById(id).orElse(null);
    }

}
