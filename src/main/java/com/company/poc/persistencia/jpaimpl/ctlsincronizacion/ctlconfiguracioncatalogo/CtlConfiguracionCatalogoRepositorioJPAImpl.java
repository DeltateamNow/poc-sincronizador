package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlconfiguracioncatalogo;

import com.company.poc.ctlsincronizacion.ctlconfiguracioncatalogos.repositorios.CtlConfiguracionCatalogoRepositorio;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Qualifier("ctlConfiguracionCatalogoRepositorioJPAImpl")
public class CtlConfiguracionCatalogoRepositorioJPAImpl implements CtlConfiguracionCatalogoRepositorio {
    private final CtlConfiguracionCatalogoJPARepo ctlConfiguracionCatalogoJPARepo;

    public CtlConfiguracionCatalogoRepositorioJPAImpl(CtlConfiguracionCatalogoJPARepo ctlConfiguracionCatalogoJPARepo) {
        this.ctlConfiguracionCatalogoJPARepo = ctlConfiguracionCatalogoJPARepo;
    }


    @Override
    public boolean catalogoActualizado(LocalDateTime dateTime, String catalogo) {
        return !ctlConfiguracionCatalogoJPARepo.findByFecUltimaActualizacionGreaterThanAndNomCatalogoDestino(dateTime, catalogo).isEmpty();
    }
}
