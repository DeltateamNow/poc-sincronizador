package com.company.poc.persistencia.jpaimpl.ctlsincronizacion.ctlsincronizaciondiaria;

import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.entidades.CtlSincronizacionesDiario;
import com.company.poc.ctlsincronizacion.ctlsincronizaciondiaria.repositorios.CtlSincronizacionDiarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("ctlSincronizacionDiarioJPARepositorioImpl")
public class CtlSincronizacionDiarioJPARepositorioImpl implements CtlSincronizacionDiarioRepositorio {
    @Autowired
    CtlSincronizacionDiarioJPARepo ctlSincronizacionDiarioJPARepo;

    @Override
    public void registrarSincronizacionDiaria(CtlSincronizacionesDiario ctlSincronizacionDiarioRepositorio) {
        ctlSincronizacionDiarioJPARepo.save(ctlSincronizacionDiarioRepositorio);
    }

    @Override
    public boolean tablaEnEjecucionConDatos(String tabla) {
        throw new UnsupportedOperationException();
    }
}
