package com.company.poc.dominio.repositorio;

import com.company.poc.dominio.catalogos.CatEmpresa;
import com.company.poc.dominio.catalogos.CatalogoBase;

import java.util.List;
import java.util.Optional;

public interface CatalogoRepositorio<T extends CatalogoBase, ID> {
    public List<T> obtenerListadoCatalogo();
    public T obtenerEntidadPorId(ID id);
}
