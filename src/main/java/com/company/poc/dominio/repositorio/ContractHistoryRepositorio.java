package com.company.poc.dominio.repositorio;

import com.company.poc.dominio.entity.Contract;
import com.company.poc.dominio.entity.ContractHistory;

import java.util.List;

public interface ContractHistoryRepositorio {
    public void save(ContractHistory contractHistory);
    void saveAll(List<ContractHistory> contractList);
}
