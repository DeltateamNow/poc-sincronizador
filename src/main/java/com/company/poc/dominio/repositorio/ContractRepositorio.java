package com.company.poc.dominio.repositorio;

import com.company.poc.dominio.entity.Contract;

import java.util.List;

public interface ContractRepositorio {
    void saveAll(List<Contract> contractList);
    public void eliminarAll(List<Contract> contractList);
}
