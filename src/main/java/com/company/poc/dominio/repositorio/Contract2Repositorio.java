package com.company.poc.dominio.repositorio;

import com.company.poc.dominio.entity.Contract;
import com.company.poc.dominio.entity.Contract2;

import java.util.List;

public interface Contract2Repositorio {
    public void saveAll(List<Contract2> contractList2);
    public void eliminarAll(List<Contract2> contractList);
}
