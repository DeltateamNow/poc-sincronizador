package com.company.poc.dominio.catalogos;

public abstract class CatalogoBase {
    public abstract Long getId();
    public abstract void setId(Long id);
    public abstract String getDescripcion();
}
