package com.company.poc.dominio.catalogos;

import javax.persistence.*;

@Table(name = "CatTienda")
@Entity
public class CatTienda extends CatalogoBase {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "CatCiudadId")
    private Long catCiudadId;

    public Long getCatCiudadId() {
        return catCiudadId;
    }

    public void setCatCiudadId(Long catCiudadId) {
        this.catCiudadId = catCiudadId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDescripcion() {
        return "";
    }
}