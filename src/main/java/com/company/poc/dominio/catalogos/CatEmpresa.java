package com.company.poc.dominio.catalogos;

import javax.persistence.*;

@Entity
@Table(name = "CatEmpresa")
public class CatEmpresa extends CatalogoBase{
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "NomEmpresa", nullable = false, length = 50)
    private String nomEmpresa;

    public String getNomEmpresa() {
        return nomEmpresa;
    }

    public void setNomEmpresa(String nomEmpresa) {
        this.nomEmpresa = nomEmpresa;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDescripcion() {
        return this.nomEmpresa;
    }

}