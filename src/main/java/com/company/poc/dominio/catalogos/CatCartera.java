package com.company.poc.dominio.catalogos;

import javax.persistence.*;

@Table(name = "CatCartera", indexes = {
        @Index(name = "UQ_CatCartera_DesCartera", columnList = "DesCartera", unique = true)
})
@Entity
public class CatCartera extends CatalogoBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @Column(name = "DesCartera", nullable = false, length = 50)
    private String desCartera;

    public String getDesCartera() {
        return desCartera;
    }

    public void setDesCartera(String desCartera) {
        this.desCartera = desCartera;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public String getDescripcion() {
        return getDesCartera();
    }
}