package com.company.poc.dominio.catalogos;

import javax.persistence.*;

@Entity
@Table(name = "CatProducto")
public class CatProducto extends CatalogoBase {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "NomProducto", nullable = false, length = 50)
    private String nomProducto;

    public String getNomProducto() {
        return nomProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public String getDescripcion() {
        return this.nomProducto;
    }
}