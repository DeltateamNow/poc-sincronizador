package com.company.poc.dominio.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Table(name = "CatCanal", indexes = {
        @Index(name = "uq_CatCanal_DesCanal", columnList = "DesCanal", unique = true)
})
@Entity
public class CatCanal extends CatalogoBase {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "DesCanal", length = 50)
    private String desCanal;

    public String getDesCanal() {
        return desCanal;
    }

    public void setDesCanal(String desCanal) {
        this.desCanal = desCanal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDescripcion() {
        return getDesCanal();
    }

}