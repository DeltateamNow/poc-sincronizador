package com.company.poc.dominio.catalogos;

import javax.persistence.*;

@Table(name = "CatTipoTransaccion",
        indexes = {
                @Index(name = "uq_CatTipoTransaccion", columnList = "DesTipoTransaccion", unique = true)
        })
@Entity
public class CatTipoTransaccion extends CatalogoBase {
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "DesTipoTransaccion", length = 50)
    private String desTipoTransaccion;

    public String getDesTipoTransaccion() {
        return desTipoTransaccion;
    }

    public void setDesTipoTransaccion(String desTipoTransaccion) {
        this.desTipoTransaccion = desTipoTransaccion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public String getDescripcion() {
        return desTipoTransaccion;
    }
}