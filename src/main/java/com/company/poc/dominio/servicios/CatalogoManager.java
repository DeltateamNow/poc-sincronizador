package com.company.poc.dominio.servicios;

import com.company.poc.dominio.catalogos.CatalogoBase;
import com.company.poc.dominio.repositorio.CatalogoRepositorio;
import org.springframework.stereotype.Service;

@Service
public class CatalogoManager {



    public Long obtenerIdPorDescripcion(String desc,
                                        CatalogoRepositorio<? extends CatalogoBase, Long> catalogoRepositorio){

        if(catalogoRepositorio != null) {
            return catalogoRepositorio.obtenerListadoCatalogo()
                    .stream()
                    .filter(entidad -> entidad.getDescripcion().equals(desc))
                    .map(entidad -> entidad.getId())
                    .findFirst().orElse(-1L);
        }else {
            return -1L;
        }
    }
}
