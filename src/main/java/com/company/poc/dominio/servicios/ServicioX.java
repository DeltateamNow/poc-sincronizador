/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.poc.dominio.servicios;

import org.springframework.stereotype.Service;

/**
 *
 * @author alvaro.ruiz
 */
@Service
public class ServicioX {
    public String getData(){
        return "Dato regresado por servicio X";
    }
    
    public int getImpuestoX(String ciudad) {
        int porcentage;
        switch(ciudad){
            case "Mexico": porcentage = 16;
            break;
            case "Laredo": porcentage = 10;
            break;
            default: porcentage = 25;
        }
        return porcentage;
    }

    public String getNombreServicio(){
        return "ServicioX";
    }
}
