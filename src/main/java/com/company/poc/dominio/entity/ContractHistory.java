package com.company.poc.dominio.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class ContractHistory implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name= "uuid2", strategy = "uuid2")
    private String contractId;
    private String holderName;
    private int duration;
    private double amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss.SSS")
    private LocalDateTime creationDate;
    private String status;

    public ContractHistory() {
    }

    public ContractHistory(String contractId, String holderName, int duration, double amount, LocalDateTime creationDate, String status) {
        this.contractId = contractId;
        this.holderName = holderName;
        this.duration = duration;
        this.amount = amount;
        this.creationDate = creationDate;
        this.status = status;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ContractHistory{" +
                "contractId='" + contractId + '\'' +
                ", holderName='" + holderName + '\'' +
                ", duration=" + duration +
                ", amount=" + amount +
                ", creationDate=" + creationDate +
                ", status='" + status + '\'' +
                '}';
    }
}
