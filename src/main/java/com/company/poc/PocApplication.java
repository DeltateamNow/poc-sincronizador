package com.company.poc;

import com.company.poc.configuracion.procesocalculo.ContextoDeAplicacion;
import com.company.poc.aplicativo.ProcesaMapaDeCalculo;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@EnableAutoConfiguration
@EnableBatchProcessing
public class PocApplication {

    public static void main(String[] args) {

        SpringApplication.run(PocApplication.class, args);
    }

}
