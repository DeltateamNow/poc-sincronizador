package com.company.poc.persistencia.impJDBC;

import com.company.poc.dominio.catalogos.CatalogoBase;

import java.util.List;

public interface CatalogoCRUDRepositorio<T extends CatalogoBase, ID> {
    public T agregarEntidad(T t);

    public T modificarEntidad(T t);

    public void removerEntidad(ID id);

    public List<T> obtenerCatalogo();

    public T obtenerEntidadPorId(ID Id);
}
