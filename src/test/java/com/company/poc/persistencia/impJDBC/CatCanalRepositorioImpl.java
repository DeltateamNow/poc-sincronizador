package com.company.poc.persistencia.impJDBC;

import com.company.poc.ctlsincronizacion.ctlagentessincronizador.entidades.CtlAgenteSincronizador;
import com.company.poc.dominio.catalogos.CatCanal;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CatCanalRepositorioImpl implements CatalogoCRUDRepositorio<CatCanal, Long> {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;
    private final RowMapper<CatCanal> rowMapper;

    public CatCanalRepositorioImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
        rowMapper = BeanPropertyRowMapper.newInstance(CatCanal.class);
    }

    @Override
    public CatCanal agregarEntidad(CatCanal catCanal) {
        CatCanal catCanal1 = null;
        Map<String, Object> param = new HashMap<>();
        param.put("DesCanal", catCanal.getDesCanal());
        jdbcTemplate.update("INSERT INTO CatCanal (DesCanal) " +
                "VALUES(:DesCanal)", param);
        catCanal1 = jdbcTemplate.queryForObject("Select * from CatCanal where DesCanal = :DesCanal ",
                param, rowMapper);
        return catCanal1;
    }

    @Override
    public CatCanal modificarEntidad(CatCanal catCanal) {
        return null;
    }

    @Override
    public void removerEntidad(Long id) {
        Map<String, Object> param = new HashMap<>();
        param.put("Id", id);
        jdbcTemplate.update("Delete from CatCanal where Id =:Id" , param);
    }

    @Override
    public List<CatCanal> obtenerCatalogo() {
        return null;
    }

    @Override
    public CatCanal obtenerEntidadPorId(Long Id) {
        return null;
    }
}
