package com.company.poc.dominio.servicios;

import com.company.poc.PocApplication;
import com.company.poc.dominio.catalogos.CatCanal;
import com.company.poc.persistencia.impJDBC.CatCanalRepositorioImpl;
import com.company.poc.persistencia.jpaimpl.dominio.CatCanalJPARepositorioImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PocApplication.class)
@TestPropertySource(properties = {"spring.config.location=classpath:test.application.yml"})
class CatalogoManagerTest {

    @Autowired
    CatCanalRepositorioImpl catCanalRepositorio;
    @Autowired
    CatCanalJPARepositorioImpl catCanalJPARepositorio;
    @Autowired
    CatalogoManager catalogoManager;
    List<Long> ids = new ArrayList<>();


    @BeforeEach
    void setUp() {
        CatCanal canal = new CatCanal();
        canal.setDesCanal("Canal1");
        ids.add(catCanalRepositorio.agregarEntidad(canal).getId());
        canal.setDesCanal("Canal2");
        ids.add(catCanalRepositorio.agregarEntidad(canal).getId());
        canal.setDesCanal("Canal3");
        ids.add(catCanalRepositorio.agregarEntidad(canal).getId());

    }

    @AfterEach
    void tearDown() {
        ids.stream().forEach(catCanalRepositorio::removerEntidad);
        ids.clear();
    }

    @Test
    void obtenerCatCanalIdPorDescripcion() {
        Long id = catalogoManager.obtenerIdPorDescripcion("Canal1", catCanalJPARepositorio);
        assertEquals(1, id);
        id = catalogoManager.obtenerIdPorDescripcion("Canal2", catCanalJPARepositorio);
        assertEquals(2, id);
    }
}